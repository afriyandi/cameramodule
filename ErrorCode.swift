//
//  ErrorCode.swift
//  O2OcameraModule
//
//  Created by Afriyandi Setiawan on 7/13/16.
//  Copyright © 2016 Afriyandi Setiawan. All rights reserved.
//

import Foundation
import UIKit

extension CGPoint {
    func scalePointByCeficient(f_x: CGFloat, f_y: CGFloat) -> CGPoint {
        return CGPoint(x: self.x/f_x, y: self.y/f_y) //original image
    }
    
    func reversePointCoordinates() -> CGPoint {
        return CGPoint(x: self.y, y: self.x)
    }
    
    func sumPointCoordinates(add: CGPoint) -> CGPoint {
        return CGPoint(x: self.x + add.x, y: self.y + add.y)
    }
    
    func substractPointCoordinates(sub: CGPoint) -> CGPoint {
        return CGPoint(x: self.x - sub.x, y: self.y - sub.y)
    }
}

class ObyRectangleFeature : NSObject {
    
    var topLeft: CGPoint!
    var topRight: CGPoint!
    var bottomLeft: CGPoint!
    var bottomRight: CGPoint!
    
    var centerPoint : CGPoint{
        get {
            let centerX = ((topLeft.x + bottomLeft.x)/2 + (topRight.x + bottomRight.x)/2)/2
            let centerY = ((topRight.y + topLeft.y)/2 + (bottomRight.y + bottomLeft.y)/2)/2
            return CGPoint(x: centerX, y: centerY)
        }
        
    }
    
    convenience init(_ rectangleFeature: CIRectangleFeature) {
        self.init()
        topLeft = rectangleFeature.topLeft
        topRight = rectangleFeature.topRight
        bottomLeft = rectangleFeature.bottomLeft
        bottomRight = rectangleFeature.bottomRight
    }
    
    override init() {
        super.init()
    }
    
    
    func rotate90Degree() -> Void {
        
        let centerPoint =  self.centerPoint
        
        //        /rotate cos(90)=0, sin(90)=1
        topLeft = CGPoint(x: centerPoint.x + (topLeft.y - centerPoint.y), y: centerPoint.y + (topLeft.x - centerPoint.x))
        topRight = CGPoint(x: centerPoint.x + (topRight.y - centerPoint.y), y: centerPoint.y + (topRight.x - centerPoint.x))
        bottomLeft = CGPoint(x: centerPoint.x + (bottomLeft.y - centerPoint.y), y: centerPoint.y + (bottomLeft.x - centerPoint.x))
        bottomRight = CGPoint(x: centerPoint.x + (bottomRight.y - centerPoint.y), y: centerPoint.y + (bottomRight.x - centerPoint.x))
    }
    
    func  scaleRectWithCoeficient(f_x: CGFloat, f_y: CGFloat) -> Void {
        topLeft =  topLeft.scalePointByCeficient(f_x, f_y: f_y)
        topRight = topRight.scalePointByCeficient(f_x, f_y: f_y)
        bottomLeft = bottomLeft.scalePointByCeficient(f_x, f_y: f_y)
        bottomRight = bottomRight.scalePointByCeficient(f_x, f_y: f_y)
    }
    
    func correctOriginPoints() -> Void {
        
        let deltaCenter = self.centerPoint.reversePointCoordinates().substractPointCoordinates(self.centerPoint)
        
        let TL = topLeft
        let TR = topRight
        let BL = bottomLeft
        let BR = bottomRight
        
        topLeft = BL.sumPointCoordinates(deltaCenter)
        topRight = TL.sumPointCoordinates(deltaCenter)
        bottomLeft = BR.sumPointCoordinates(deltaCenter)
        bottomRight = TR.sumPointCoordinates(deltaCenter)
    }
}

func analyzeImage(image: UIImage) -> [Quadrilateral]
{
    guard let ciImage = CIImage(image: image)
        else { return [] }
    
    let flip = true // set to false to prevent flipping the coordinates
    
    let context = CIContext(options: nil)
    
    let detector = CIDetector(ofType: CIDetectorTypeRectangle, context: context, options: [CIDetectorAccuracy:CIDetectorAccuracyHigh])
    
    let features = detector.featuresInImage(ciImage)
    
    UIGraphicsBeginImageContext(ciImage.extent.size)
    let currentContext = UIGraphicsGetCurrentContext()
    
    var frames: [Quadrilateral] = []
    
    for feature in features as! [CIRectangleFeature]
    {
        var topLeft = CGContextConvertPointToUserSpace(currentContext, feature.topLeft)
        var topRight = CGContextConvertPointToUserSpace(currentContext, feature.topRight)
        var bottomRight = CGContextConvertPointToUserSpace(currentContext, feature.bottomRight)
        var bottomLeft = CGContextConvertPointToUserSpace(currentContext, feature.bottomLeft)
        
        if flip {
            topLeft = CGPoint(x: topLeft.x, y: image.size.height - topLeft.y)
            topRight = CGPoint(x: topRight.x, y: image.size.height - topRight.y)
            bottomLeft = CGPoint(x: bottomLeft.x, y: image.size.height - bottomLeft.y)
            bottomRight = CGPoint(x: bottomRight.x, y: image.size.height - bottomRight.y)
        }
        
        let frame = Quadrilateral(topLeft: topLeft, topRight: topRight, bottomLeft: bottomLeft, bottomRight: bottomRight)
        
        frames.append(frame)
    }
    
    UIGraphicsEndImageContext()
    return frames
}

struct Quadrilateral {
    var topLeft : CGPoint = CGPointZero
    var topRight : CGPoint = CGPointZero
    var bottomLeft : CGPoint = CGPointZero
    var bottomRight : CGPoint = CGPointZero
    
    var path : UIBezierPath {
        get {
            let tempPath = UIBezierPath()
            tempPath.moveToPoint(topLeft)
            tempPath.addLineToPoint(topRight)
            tempPath.addLineToPoint(bottomRight)
            tempPath.addLineToPoint(bottomLeft)
            tempPath.addLineToPoint(topLeft)
            return tempPath
        }
    }
    
    init(topLeft topLeft_I: CGPoint, topRight topRight_I: CGPoint, bottomLeft bottomLeft_I: CGPoint, bottomRight bottomRight_I: CGPoint) {
        topLeft = topLeft_I
        topRight = topRight_I
        bottomLeft = bottomLeft_I
        bottomRight = bottomRight_I
    }
    
    var frame : CGRect {
        get {
            let highestPoint = max(topLeft.y, topRight.y, bottomLeft.y, bottomRight.y)
            let lowestPoint = min(topLeft.y, topRight.y, bottomLeft.y, bottomRight.y)
            let farthestPoint = max(topLeft.x, topRight.x, bottomLeft.x, bottomRight.x)
            let closestPoint = min(topLeft.x, topRight.x, bottomLeft.x, bottomRight.x)
            
            // you might want to set origin to (0,0)
            let origin = CGPoint(x: closestPoint, y: lowestPoint)
            let size = CGSize(width: farthestPoint, height: highestPoint)
            
            return CGRect(origin: origin, size: size)
        }
    }
    
    var size : CGSize {
        get {
            return frame.size
        }
    }
    
    var origin : CGPoint {
        get {
            return frame.origin
        }
    }
}
