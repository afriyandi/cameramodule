protocol FoodConsumer {
    var calorieCount: Double { get set }
    var hydrationLevel: Double { get set }
}

protocol Food {
    func beConsumedBy<T: FoodConsumer>(consumer: T, grams: Double) -> T
}

extension FoodConsumer {
    func eat(food: Food, grams: Double) -> Self {
        return food.beConsumedBy(self, grams: grams)
    }
}


struct Cat: FoodConsumer {
    var calorieCount: Double = 0
    var hydrationLevel: Double = 0
}

struct Kibble: Food {
    let caloriesPerGram: Double = 40
    
    func beConsumedBy<T: FoodConsumer>(consumer: T, grams: Double) -> T {
        var newConsumer = consumer
        newConsumer.calorieCount += grams * caloriesPerGram
        return newConsumer
    }
}

struct FancyFeast: Food {
    let caloriesPerGram: Double = 80
    let milliLitresWaterPerGram: Double = 0.2
    
    func beConsumedBy<T: FoodConsumer>(consumer: T, grams: Double) -> T {
        var newConsumer = consumer
        newConsumer.calorieCount += grams * caloriesPerGram
        newConsumer.hydrationLevel += grams * milliLitresWaterPerGram
        return newConsumer
    }
}


let catFood = Kibble()
let wetFood = FancyFeast()
var dave = Cat()

dave = dave.eat(catFood, grams: 30)
dave = dave.eat(wetFood, grams: 20)
print(dave)

public final class OnDelete {
    var closure: () -> Void
    public init(_ c: () -> Void) {
        closure = c
    }
    deinit {
        closure()
    }
}

struct Counter {
    var count = 0
    var od: OnDelete? = nil
    static func construct() -> () -> () {
        var c = Counter()
        c.od = OnDelete{
            print("Value loop break is \(c.count)")
        }
        return {
            c.count += 1
            c.od = nil
        }
    }
}

do {
    var loopBreaker = Counter.construct()
    loopBreaker()
}