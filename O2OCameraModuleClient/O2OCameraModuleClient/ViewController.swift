//
//  ViewController.swift
//  O2OCameraModuleClient
//
//  Created by Afriyandi Setiawan on 6/27/16.
//  Copyright © 2016 Afriyandi Setiawan. All rights reserved.
//

import UIKit
import O2OcameraModule

class ViewController: UIViewController {
    
    @IBOutlet weak var previewTemplate: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let s = UIStoryboard (
            name: "O2OCam", bundle: NSBundle(forClass: O2OCamView.self)
        )
        let vc:O2OCamView = s.instantiateViewControllerWithIdentifier("O2OCam") as! O2OCamView
        //workaround for presentviewcontroller delay
//        vc.mode = .QR
        vc.delegate = self
        UIView.transitionWithView(vc.view, duration: 0.5, options: .CurveEaseOut, animations: { () -> Void in
            self.view.window?.addSubview(vc.view)
            }, completion: nil)
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.presentViewController(vc, animated: false, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension ViewController: O2OCamViewDelegate {
    func sendImage(image:UIImage) {
        self.previewTemplate.image = image
    }
}