////
////  Croop.swift
////  O2OcameraModule
////
////  Created by Afriyandi Setiawan on 8/8/16.
////  Copyright © 2016 Afriyandi Setiawan. All rights reserved.
////
//
//import UIKit
//
//// MARK: ControlPointView interface
//
//class ControlPointView: UIView {
//    var red: CGFloat
//    var green: CGFloat!
//    var blue: CGFloat!
//    var alphaC: CGFloat!
//    
//    var color: UIColor {
//        get {
//            return UIColor(red: red, green: green, blue: blue, alpha: alpha)
//        }
//        set(_color) {
//            color.getRed(red, green: green, blue: blue, alpha: alpha)
//            self.setNeedsDisplay()
//        }
//    }
//    
//    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        if self != nil {
//            // Initialization code
//            self.color = UIColor(red: 18.0 / 255.0, green: 173.0 / 255.0, blue: 251.0 / 255.0, alpha: 1)
//            self.opaque = false
//        }
//    }
//    
//    override func drawRect(rect: CGRect) {
//        var context: CGContextRef = UIGraphicsGetCurrentContext()
//        CGContextClearRect(context, rect)
//        CGContextSetRGBFillColor(context, red, green, blue, alpha)
//        CGContextFillEllipseInRect(context, rect)
//    }
//}
//
//// MARK: ShadeView interface
//
//class ShadeView: UIView {
//    var cropBorderRed: CGFloat
//    var cropBorderGreen: CGFloat
//    var cropBorderBlue: CGFloat
//    var cropBorderAlpha: CGFloat
//    
//    var cropBorderColor: UIColor {
//        get {
//            return UIColor(red: cropBorderRed, green: cropBorderGreen, blue: cropBorderBlue, alpha: cropBorderAlpha)
//        }
//        set(_color) {
//            color.getRed(cropBorderRed, green: cropBorderGreen, blue: cropBorderBlue, alpha: cropBorderAlpha)
//            self.setNeedsDisplay()
//        }
//    }
//    
//    var cropArea: CGRect {
//        get {
//            if self.cropView {
//                return self.cropView.cropAreaInImage
//            }
//            else {
//                return CGRectZero
//            }
//        }
//        set(cropArea) {
//            self.cropArea = cropArea
//            if self.cropView {
//                self.cropView.cropAreaInImage = cropArea
//            }
//        }
//    }
//    
//    var shadeAlpha: CGFloat {
//        get {
//            return shadeAlpha
//        }
//        set(_alpha) {
//            shadeAlpha = alpha
//            self.setNeedsDisplay()
//        }
//    }
//    
//    var blurredImageView: UIImageView
//    
//    
//    init(frame: CGRect) {
//        super.init(frame: frame)
//        if self != nil {
//            // Initialization code
//            self.opaque = false
//            self.blurredImageView = UIImageView()
//        }
//    }
//    
//    func setCropArea(cropArea: CGRect) {
//        cropArea = cropArea
//        self.setNeedsDisplay()
//    }
//    
//    func cropArea() -> CGRect {
//        return cropArea
//    }
//    
//    override func drawRect(rect: CGRect) {
//        var layer: CALayer = self.blurredImageView.layer
//        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
//        var c: CGContextRef = UIGraphicsGetCurrentContext()
//        CGContextAddRect(c, self.cropArea)
//        CGContextAddRect(c, rect)
//        CGContextEOClip(c)
//        CGContextSetFillColorWithColor(c, UIColor.blackColor().CGColor)
//        CGContextFillRect(c, rect)
//        var maskim: UIImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        var mask: CALayer = CALayer.layer
//        mask.frame = rect
//        mask.contents = (maskim.CGImage as! AnyObject)
//        layer.mask = mask
//    }
//}
//var SquareCGRectAtCenter: CGRect
//
//var dragView: UIView
//
//var DragPoint: struct{CGPointdragStart;CGPointtopLeftCenter;CGPointbottomLeftCenter;CGPointbottomRightCenter;CGPointtopRightCenter;CGPointcropAreaCenter;}
//
//// Used when working with multiple dragPoints
//var MultiDragPoint: struct{DragPointmainPoint;DragPointoptionalPoint;NSUIntegerlastCount;}
//
//
//// MARK: ImageCropView interface
//
//class ImageCropView: UIView {
//    var imageView: UIImageView
//    var imageFrameInView: CGRect
//    var topLeftPoint: ControlPointView
//    var bottomLeftPoint: ControlPointView
//    var bottomRightPoint: ControlPointView
//    var topRightPoint: ControlPointView
//    var PointsArray: [AnyObject]
//    var cropAreaView: UIView
//    var dragPoint: DragPoint
//    var multiDragPoint: MultiDragPoint
//    var dragViewOne: UIView
//    var dragViewTwo: UIView
//    
//    init(frame: CGRect, blurOn: Bool) {
//        super.init(frame: frame)
//        if self != nil {
//            self.blurred = blurOn
//            self.initViews()
//        }
//    }
//    
//    func setImage(image: UIImage) {
//        var frameWidth: CGFloat = self.frame.size.width
//        var frameHeight: CGFloat = self.frame.size.height
//        var imageWidth: CGFloat = image.size.width
//        var imageHeight: CGFloat = image.size.height
//        var isPortrait: Bool = imageHeight / frameHeight > imageWidth / frameWidth
//        var x: Int
//        var y: Int
//        var scaledImageWidth: Int
//        var scaledImageHeight: Int
//        if isPortrait {
//            imageScale = imageHeight / frameHeight
//            scaledImageWidth = imageWidth / imageScale
//            scaledImageHeight = frameHeight
//            x = (frameWidth - scaledImageWidth) / 2
//            y = 0
//        }
//        else {
//            imageScale = imageWidth / frameWidth
//            scaledImageWidth = frameWidth
//            scaledImageHeight = imageHeight / imageScale
//            x = 0
//            y = (frameHeight - scaledImageHeight) / 2
//        }
//        imageFrameInView = CGRectMake(x, y, scaledImageWidth, scaledImageHeight)
//        imageView.frame = imageFrameInView
//        imageView.image = image
//        /* prepare imageviews and their frame */
//        self.shadeView.blurredImageView.contentMode = .ScaleAspectFill
//        imageView.contentMode = .ScaleAspectFill
//        imageView.clipsToBounds = true
//        self.shadeView.blurredImageView.clipsToBounds = true
//        var blurFrame: CGRect
//        if imageFrameInView.origin.x < 0 && (imageFrameInView.size.width - fabs(imageFrameInView.origin.x) >= 320) {
//            blurFrame = self.frame
//        }
//        else {
//            blurFrame = imageFrameInView
//        }
//        imageView.frame = imageFrameInView
//        // blurredimageview is on top of shadeview so shadeview needs the same frame as imageView.
//        self.shadeView.frame = imageFrameInView
//        self.shadeView.blurredImageView.frame = blurFrame
//        // perform image blur
//        UIImage * blur
//        if self.blurred {
//            blur = image.blurredImageWithRadius(30, iterations: 1, tintColor: UIColor.blackColor())
//        }
//        else {
//            blur = image.blurredImageWithRadius(0, iterations: 1, tintColor: UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0))
//        }
//        self.shadeView.blurredImageView.image = blur
//        //Special fix. If scaledImageWidth or scaledImageHeight < cropArea.width of cropArea.Height.
//        self.boundingBoxForTopLeft(topLeftPoint.center, bottomLeft: bottomLeftPoint.center, bottomRight: bottomRightPoint.center, topRight: topRightPoint.center)
//        var cropArea: CGRect = self.cropAreaFromControlPoints()
//        cropAreaView.frame = cropArea
//        cropArea.origin.y = cropArea.origin.y - imageFrameInView.origin.y
//        cropArea.origin.x = cropArea.origin.x - imageFrameInView.origin.x
//        self.shadeView.cropArea = cropArea
//    }
//    var controlPointSize: CGFloat {
//        get {
//            return controlPointSize
//        }
//        set(_controlPointSize) {
//            var halfSize: CGFloat = controlPointSize
//            var topLeftPointFrame: CGRect = CGRectMake(topLeftPoint.center.x - halfSize, topLeftPoint.center.y - halfSize, controlPointSize, controlPointSize)
//            var bottomLeftPointFrame: CGRect = CGRectMake(bottomLeftPoint.center.x - halfSize, bottomLeftPoint.center.y - halfSize, controlPointSize, controlPointSize)
//            var bottomRightPointFrame: CGRect = CGRectMake(bottomRightPoint.center.x - halfSize, bottomRightPoint.center.y - halfSize, controlPointSize, controlPointSize)
//            var topRightPointFrame: CGRect = CGRectMake(topRightPoint.center.x - halfSize, topRightPoint.center.y - halfSize, controlPointSize, controlPointSize)
//            topLeftPoint.frame = topLeftPointFrame
//            bottomLeftPoint.frame = bottomLeftPointFrame
//            bottomRightPoint.frame = bottomRightPointFrame
//            topRightPoint.frame = topRightPointFrame
//            self.setNeedsDisplay()
//        }
//    }
//    
//    var image: UIImage {
//        get {
//            return self.image
//        }
//        set(image) {
//            var frameWidth: CGFloat = self.frame.size.width
//            var frameHeight: CGFloat = self.frame.size.height
//            var imageWidth: CGFloat = image.size.width
//            var imageHeight: CGFloat = image.size.height
//            var isPortrait: Bool = imageHeight / frameHeight > imageWidth / frameWidth
//            var x: Int
//            var y: Int
//            var scaledImageWidth: Int
//            var scaledImageHeight: Int
//            if isPortrait {
//                imageScale = imageHeight / frameHeight
//                scaledImageWidth = imageWidth / imageScale
//                scaledImageHeight = frameHeight
//                x = (frameWidth - scaledImageWidth) / 2
//                y = 0
//            }
//            else {
//                imageScale = imageWidth / frameWidth
//                scaledImageWidth = frameWidth
//                scaledImageHeight = imageHeight / imageScale
//                x = 0
//                y = (frameHeight - scaledImageHeight) / 2
//            }
//            imageFrameInView = CGRectMake(x, y, scaledImageWidth, scaledImageHeight)
//            imageView.frame = imageFrameInView
//            imageView.image = image
//            /* prepare imageviews and their frame */
//            self.shadeView.blurredImageView.contentMode = .ScaleAspectFill
//            imageView.contentMode = .ScaleAspectFill
//            imageView.clipsToBounds = true
//            self.shadeView.blurredImageView.clipsToBounds = true
//            var blurFrame: CGRect
//            if imageFrameInView.origin.x < 0 && (imageFrameInView.size.width - fabs(imageFrameInView.origin.x) >= 320) {
//                blurFrame = self.frame
//            }
//            else {
//                blurFrame = imageFrameInView
//            }
//            imageView.frame = imageFrameInView
//            // blurredimageview is on top of shadeview so shadeview needs the same frame as imageView.
//            self.shadeView.frame = imageFrameInView
//            self.shadeView.blurredImageView.frame = blurFrame
//            // perform image blur
//            UIImage * blur
//            if self.blurred {
//                blur = image.blurredImageWithRadius(30, iterations: 1, tintColor: UIColor.blackColor())
//            }
//            else {
//                blur = image.blurredImageWithRadius(0, iterations: 1, tintColor: UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0))
//            }
//            self.shadeView.blurredImageView.image = blur
//            //Special fix. If scaledImageWidth or scaledImageHeight < cropArea.width of cropArea.Height.
//            self.boundingBoxForTopLeft(topLeftPoint.center, bottomLeft: bottomLeftPoint.center, bottomRight: bottomRightPoint.center, topRight: topRightPoint.center)
//            var cropArea: CGRect = self.cropAreaFromControlPoints()
//            cropAreaView.frame = cropArea
//            cropArea.origin.y = cropArea.origin.y - imageFrameInView.origin.y
//            cropArea.origin.x = cropArea.origin.x - imageFrameInView.origin.x
//            self.shadeView.cropArea = cropArea
//        }
//    }
//    
//    var cropAreaInView: CGRect {
//        get {
//            var cropArea: CGRect = self.cropAreaFromControlPoints()
//            return cropArea
//        }
//        set(cropArea) {
//            var topLeft: CGPoint = cropArea.origin
//            var bottomLeft: CGPoint = CGPointMake(cropArea.origin.x, cropArea.origin.y + cropArea.size.height)
//            var bottomRight: CGPoint = CGPointMake(cropArea.origin.x + cropArea.size.width, cropArea.origin.y + cropArea.size.height)
//            var topRight: CGPoint = CGPointMake(cropArea.origin.x + cropArea.size.width, cropArea.origin.y)
//            topLeftPoint.center = topLeft
//            bottomLeftPoint.center = bottomLeft
//            bottomRightPoint.center = bottomRight
//            topRightPoint.center = topRight
//            self.cropAreaForViews = cropArea
//            self.setNeedsDisplay()
//        }
//    }
//    
//    var cropAreaInImage: CGRect {
//        get {
//            var cropArea: CGRect = self.cropAreaInView
//            var r: CGRect = CGRectMake(Int((cropArea.origin.x - imageFrameInView.origin.x) * self.imageScale), Int((cropArea.origin.y - imageFrameInView.origin.y) * self.imageScale), Int(cropArea.size.width * self.imageScale), Int(cropArea.size.height * self.imageScale))
//            return r
//        }
//        set(_cropAreaInImage) {
//            var r: CGRect = CGRectMake(cropAreaInImage.origin.x / self.imageScale + imageFrameInView.origin.x, cropAreaInImage.origin.y / self.imageScale + imageFrameInView.origin.y, cropAreaInImage.size.width / self.imageScale, cropAreaInImage.size.height / self.imageScale)
//            self.cropAreaInView = r
//        }
//    }
//    
//    var imageScale: CGFloat {
//        get {
//            return self.imageScale
//        }
//    }
//    
//    var maskAlpha: CGFloat {
//        get {
//            return self.shadeView.shadeAlpha
//        }
//        set(alpha) {
//            self.shadeView.shadeAlpha = alpha
//        }
//    }
//    
//    var controlColor: UIColor {
//        get {
//            return controlColor
//        }
//        set(_color) {
//            controlColor = color
//            self.shadeView.cropBorderColor = color
//            topLeftPoint.color = color
//            bottomLeftPoint.color = color
//            bottomRightPoint.color = color
//            topRightPoint.color = color
//        }
//    }
//    
//    var shadeView: ShadeView
//    var blurred: Bool
//    
//    
//    init(frame: CGRect) {
//        super.init(frame: frame)
//        if self != nil {
//            self.initViews()
//        }
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        
//        self.initViews()
//    }
//    
//    func initViews() {
//        var subviewFrame: CGRect = self.bounds
//        //the shade
//        self.shadeView = ShadeView(frame: subviewFrame)
//        //the image
//        imageView = UIImageView(frame: subviewFrame)
//        imageView.contentMode = .ScaleAspectFill
//        //control points
//        controlPointSize = DEFAULT_CONTROL_POINT_SIZE
//        var initialCropAreaSize: Int = self.frame.size.width / 5
//        var centerInView: CGPoint = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2)
//        topLeftPoint = self.createControlPointAt(SquareCGRectAtCenter(centerInView.x - initialCropAreaSize, centerInView.y - initialCropAreaSize, controlPointSize))
//        bottomLeftPoint = self.createControlPointAt(SquareCGRectAtCenter(centerInView.x - initialCropAreaSize, centerInView.y + initialCropAreaSize, controlPointSize))
//        bottomRightPoint = self.createControlPointAt(SquareCGRectAtCenter(centerInView.x + initialCropAreaSize, centerInView.y + initialCropAreaSize, controlPointSize))
//        topRightPoint = self.createControlPointAt(SquareCGRectAtCenter(centerInView.x + initialCropAreaSize, centerInView.y - initialCropAreaSize, controlPointSize))
//        //the "hole"
//        var cropArea: CGRect = self.cropAreaFromControlPoints()
//        cropAreaView = UIView(frame: cropArea)
//        cropAreaView.opaque = false
//        cropAreaView.backgroundColor = UIColor.clearColor()
//        var dragRecognizer: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.handleDrag))
//        dragRecognizer.view.multipleTouchEnabled = true
//        dragRecognizer.minimumNumberOfTouches = 1
//        dragRecognizer.maximumNumberOfTouches = 2
//        self.viewForFirstBaselineLayout.addGestureRecognizer(dragRecognizer)
//        self.addSubview(imageView)
//        self.addSubview(self.shadeView)
//        self.addSubview(self.shadeView.blurredImageView)
//        self.addSubview(cropAreaView)
//        self.addSubview(topRightPoint)
//        self.addSubview(bottomRightPoint)
//        self.addSubview(topLeftPoint)
//        self.addSubview(bottomLeftPoint)
//        PointsArray = [topRightPoint, bottomRightPoint, topLeftPoint, bottomLeftPoint]
//        self.maskAlpha = DEFAULT_MASK_ALPHA
//        imageFrameInView = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)
//        imageView.frame = imageFrameInView
//    }
//    
//    func createControlPointAt(frame: CGRect) -> ControlPointView {
//        var point: ControlPointView = ControlPointView(frame: frame)
//        return point
//    }
//    
//    func cropAreaFromControlPoints() -> CGRect {
//        var width: CGFloat = topRightPoint.center.x - topLeftPoint.center.x
//        var height: CGFloat = bottomRightPoint.center.y - topRightPoint.center.y
//        var hole: CGRect = CGRectMake(topLeftPoint.center.x, topLeftPoint.center.y, width, height)
//        return hole
//    }
//    
//    func controllableAreaFromControlPoints() -> CGRect {
//        var width: CGFloat = topRightPoint.center.x - topLeftPoint.center.x - controlPointSize
//        var height: CGFloat = bottomRightPoint.center.y - topRightPoint.center.y - controlPointSize
//        var hole: CGRect = CGRectMake(topLeftPoint.center.x + controlPointSize / 2, topLeftPoint.center.y + controlPointSize / 2, width, height)
//        return hole
//    }
//    
//    func boundingBoxForTopLeft(topLeft: CGPoint, bottomLeft: CGPoint, bottomRight: CGPoint, topRight: CGPoint) {
//        var box: CGRect = CGRectMake(topLeft.x - controlPointSize / 2, topLeft.y - controlPointSize / 2, topRight.x - topLeft.x + controlPointSize, bottomRight.y - topRight.y + controlPointSize)
//        //If not square - crop cropView =-)
//        if !square {
//            box = CGRectIntersection(imageFrameInView, box)
//        }
//        if CGRectContainsRect(imageFrameInView, box) {
//            bottomLeftPoint.center = CGPointMake(box.origin.x + controlPointSize / 2, box.origin.y + box.size.height - controlPointSize / 2)
//            bottomRightPoint.center = CGPointMake(box.origin.x + box.size.width - controlPointSize / 2, box.origin.y + box.size.height - controlPointSize / 2)
//            topLeftPoint.center = CGPointMake(box.origin.x + controlPointSize / 2, box.origin.y + controlPointSize / 2)
//            topRightPoint.center = CGPointMake(box.origin.x + box.size.width - controlPointSize / 2, box.origin.y + controlPointSize / 2)
//        }
//    }
//    
//    func checkHit(point: CGPoint) -> UIView? {
//        var view: UIView = cropAreaView
//        for i in 0..<PointsArray.count {
//            var x: CGFloat = (PointsArray[i] as! ControlPointView).center().x
//            var y: CGFloat = (PointsArray[i] as! ControlPointView).center().y
//            if sqrt(pow((point.x - view.center.x), 2) + pow((point.y - view.center.y), 2)) > sqrt(pow((point.x - x), 2) + pow((point.y - y), 2)) {
//                view = PointsArray[i]
//            }
//        }
//        return view
//    }
//    // Overriding this method to create a larger touch surface area without changing view
//    
//    func hitTest(point: CGPoint, withEvent event: UIEvent) -> UIView? {
//        var frame: CGRect = CGRectInset(cropAreaView.frame, -30, -30)
//        return CGRectContainsPoint(frame, point) ? cropAreaView : nil
//    }
//    
//    func handleDrag(recognizer: UIPanGestureRecognizer) {
//        var count: Int = recognizer.numberOfTouches()
//        if recognizer.state == .Began || multiDragPoint.lastCount != count {
//            if count > 1 {
//                self.prepMultiTouchPan(recognizer, withCount: count)
//            }
//            else {
//                self.prepSingleTouchPan(recognizer)
//            }
//            multiDragPoint.lastCount = count
//            return
//        }
//        else if recognizer.state == .Ended {
//            return
//            // no-op
//            // no-op
//        }
//        
//        if count > 1 {
//            // Transforms crop box based on the two dragPoints.
//            for i in 0..<count {
//                dragPoint = i == 0 ? multiDragPoint.mainPoint : multiDragPoint.optionalPoint
//                self.beginCropBoxTransformForPoint(recognizer.locationOfTouch(i, inView: self), atView: (i == 0 ? dragViewOne : dragViewTwo))
//                // Assign point centers that could have changed in previous transform
//                multiDragPoint.optionalPoint.topLeftCenter = topLeftPoint.center
//                multiDragPoint.optionalPoint.bottomLeftCenter = bottomLeftPoint.center
//                multiDragPoint.optionalPoint.bottomRightCenter = bottomRightPoint.center
//                multiDragPoint.optionalPoint.topRightCenter = topRightPoint.center
//                multiDragPoint.optionalPoint.cropAreaCenter = cropAreaView.center
//            }
//        }
//        else {
//            self.beginCropBoxTransformForPoint(recognizer.locationInView(self), atView: dragViewOne)
//        }
//        // Used to reset multiDragPoint when changing from 1 to 2 touches.
//        multiDragPoint.lastCount = count
//    }
//    /**
//     * Records current values and points for multi-finger pan gestures
//     * @params recognizer The pan gesuture with current point values
//     * @params count The number of touches on view
//     */
//    
//    func prepMultiTouchPan(recognizer: UIPanGestureRecognizer, withCount count: Int) {
//        for i in 0..<count {
//            if i == 0 {
//                dragViewOne = self.checkHit(recognizer.locationOfTouch(i, inView: self))
//                multiDragPoint.mainPoint.dragStart = recognizer.locationOfTouch(i, inView: self)
//            }
//            else {
//                dragViewTwo = self.checkHit(recognizer.locationOfTouch(i, inView: self))
//                multiDragPoint.optionalPoint.dragStart = recognizer.locationOfTouch(i, inView: self)
//            }
//        }
//        multiDragPoint.mainPoint.topLeftCenter = topLeftPoint.center
//        multiDragPoint.mainPoint.bottomLeftCenter = bottomLeftPoint.center
//        multiDragPoint.mainPoint.bottomRightCenter = bottomRightPoint.center
//        multiDragPoint.mainPoint.topRightCenter = topRightPoint.center
//        multiDragPoint.mainPoint.cropAreaCenter = cropAreaView.center
//    }
//    /**
//     * Records current values and points for single finger pan gestures
//     * @params recognizer The pan gesuture with current point values
//     */
//    
//    func prepSingleTouchPan(recognizer: UIPanGestureRecognizer) {
//        dragViewOne = self.checkHit(recognizer.locationInView(self))
//        dragPoint.dragStart = recognizer.locationInView(self)
//        dragPoint.topLeftCenter = topLeftPoint.center
//        dragPoint.bottomLeftCenter = bottomLeftPoint.center
//        dragPoint.bottomRightCenter = bottomRightPoint.center
//        dragPoint.topRightCenter = topRightPoint.center
//        dragPoint.cropAreaCenter = cropAreaView.center
//    }
//    
//    func setCropAreaForViews(cropArea: CGRect) {
//        cropAreaView.frame = cropArea
//        // Create offset to make frame within imageView
//        cropArea.origin.y = cropArea.origin.y - imageFrameInView.origin.y
//        cropArea.origin.x = cropArea.origin.x - imageFrameInView.origin.x
//        self.shadeView.cropArea = cropArea
//    }
//    
//    func beginCropBoxTransformForPoint(location: CGPoint, atView view: UIView) {
//        if view == topLeftPoint {
//            self.handleDragTopLeft(location)
//        }
//        else if view == bottomLeftPoint {
//            self.handleDragBottomLeft(location)
//        }
//        else if view == bottomRightPoint {
//            self.handleDragBottomRight(location)
//        }
//        else if view == topRightPoint {
//            self.handleDragTopRight(location)
//        }
//        else if view == cropAreaView {
//            self.handleDragCropArea(location)
//        }
//        
//        var cropArea: CGRect = self.cropAreaFromControlPoints()
//        self.cropAreaForViews = cropArea
//    }
//    
//    func deriveDisplacementFromDragLocation(dragLocation: CGPoint, draggedPoint: CGPoint, oppositePoint: CGPoint) -> CGSize {
//        var dX: CGFloat = dragLocation.x - dragPoint.dragStart.x
//        var dY: CGFloat = dragLocation.y - dragPoint.dragStart.y
//        var tempDraggedPoint: CGPoint = CGPointMake(draggedPoint.x + dX, draggedPoint.y + dY)
//        var width: CGFloat = (tempDraggedPoint.x - oppositePoint.x)
//        var height: CGFloat = (tempDraggedPoint.y - oppositePoint.y)
//        var size: CGFloat = fabs(width) >= fabs(height) ? width : height
//        var xDir: CGFloat = draggedPoint.x <= oppositePoint.x ? 1 : -1
//        var yDir: CGFloat = draggedPoint.y <= oppositePoint.y ? 1 : -1
//        var newX: CGFloat = 0
//        var newY: CGFloat = 0
//        if xDir >= 0 {
//            //on the right
//            if square {
//                newX = oppositePoint.x - fabs(size)
//            }
//            else {
//                newX = oppositePoint.x - fabs(width)
//            }
//        }
//        else {
//            //on the left
//            if square {
//                newX = oppositePoint.x + fabs(size)
//            }
//            else {
//                newX = oppositePoint.x + fabs(width)
//            }
//        }
//        if yDir >= 0 {
//            //on the top
//            if square {
//                newY = oppositePoint.y - fabs(size)
//            }
//            else {
//                newY = oppositePoint.y - fabs(height)
//            }
//        }
//        else {
//            //on the bottom
//            if square {
//                newY = oppositePoint.y + fabs(size)
//            }
//            else {
//                newY = oppositePoint.y + fabs(height)
//            }
//        }
//        var displacement: CGSize = CGSizeMake(newX - draggedPoint.x, newY - draggedPoint.y)
//        return displacement
//    }
//    
//    func handleDragTopLeft(dragLocation: CGPoint) {
//        var disp: CGSize = self.deriveDisplacementFromDragLocation(dragLocation, draggedPoint: dragPoint.topLeftCenter, oppositePoint: dragPoint.bottomRightCenter)
//        var topLeft: CGPoint = CGPointMake(dragPoint.topLeftCenter.x + disp.width, dragPoint.topLeftCenter.y + disp.height)
//        var topRight: CGPoint = CGPointMake(dragPoint.topRightCenter.x, dragPoint.topLeftCenter.y + disp.height)
//        var bottomLeft: CGPoint = CGPointMake(dragPoint.bottomLeftCenter.x + disp.width, dragPoint.bottomLeftCenter.y)
//        // Make sure that the new cropping area will not be smaller than the minimum image size
//        var width: CGFloat = topRight.x - topLeft.x
//        var height: CGFloat = bottomLeft.y - topLeft.y
//        width = width * self.imageScale
//        height = height * self.imageScale
//        // If the crop area is too small, set the points at the minimum spacing.
//        var cropArea: CGRect = self.cropAreaFromControlPoints()
//        if width >= IMAGE_MIN_WIDTH && height < IMAGE_MIN_HEIGHT {
//            topLeft.y = cropArea.origin.y + (((cropArea.size.height * self.imageScale) - IMAGE_MIN_HEIGHT) / self.imageScale)
//            topRight.y = topLeft.y
//        }
//        else if width < IMAGE_MIN_WIDTH && height >= IMAGE_MIN_HEIGHT {
//            topLeft.x = cropArea.origin.x + (((cropArea.size.width * self.imageScale) - IMAGE_MIN_WIDTH) / self.imageScale)
//            bottomLeft.x = topLeft.x
//        }
//        else if width < IMAGE_MIN_WIDTH && height < IMAGE_MIN_HEIGHT {
//            topLeft.x = cropArea.origin.x + (((cropArea.size.width * self.imageScale) - IMAGE_MIN_WIDTH) / self.imageScale)
//            topLeft.y = cropArea.origin.y + (((cropArea.size.height * self.imageScale) - IMAGE_MIN_HEIGHT) / self.imageScale)
//            topRight.y = topLeft.y
//            bottomLeft.x = topLeft.x
//        }
//        
//        self.boundingBoxForTopLeft(topLeft, bottomLeft: bottomLeft, bottomRight: dragPoint.bottomRightCenter, topRight: topRight)
//    }
//    
//    func handleDragBottomLeft(dragLocation: CGPoint) {
//        var disp: CGSize = self.deriveDisplacementFromDragLocation(dragLocation, draggedPoint: dragPoint.bottomLeftCenter, oppositePoint: dragPoint.topRightCenter)
//        var bottomLeft: CGPoint = CGPointMake(dragPoint.bottomLeftCenter.x + disp.width, dragPoint.bottomLeftCenter.y + disp.height)
//        var topLeft: CGPoint = CGPointMake(dragPoint.topLeftCenter.x + disp.width, dragPoint.topLeftCenter.y)
//        var bottomRight: CGPoint = CGPointMake(dragPoint.bottomRightCenter.x, dragPoint.bottomRightCenter.y + disp.height)
//        // Make sure that the new cropping area will not be smaller than the minimum image size
//        var width: CGFloat = bottomRight.x - bottomLeft.x
//        var height: CGFloat = bottomLeft.y - topLeft.y
//        width = width * self.imageScale
//        height = height * self.imageScale
//        // If the crop area is too small, set the points at the minimum spacing.
//        var cropArea: CGRect = self.cropAreaFromControlPoints()
//        if width >= IMAGE_MIN_WIDTH && height < IMAGE_MIN_HEIGHT {
//            bottomLeft.y = cropArea.origin.y + (IMAGE_MIN_HEIGHT / self.imageScale)
//            bottomRight.y = bottomLeft.y
//        }
//        else if width < IMAGE_MIN_WIDTH && height >= IMAGE_MIN_HEIGHT {
//            bottomLeft.x = cropArea.origin.x + (((cropArea.size.width * self.imageScale) - IMAGE_MIN_WIDTH) / self.imageScale)
//            topLeft.x = bottomLeft.x
//        }
//        else if width < IMAGE_MIN_WIDTH && height < IMAGE_MIN_HEIGHT {
//            bottomLeft.x = cropArea.origin.x + (((cropArea.size.width * self.imageScale) - IMAGE_MIN_WIDTH) / self.imageScale)
//            bottomLeft.y = cropArea.origin.y + (IMAGE_MIN_HEIGHT / self.imageScale)
//            topLeft.x = bottomLeft.x
//            bottomRight.y = bottomLeft.y
//        }
//        
//        self.boundingBoxForTopLeft(topLeft, bottomLeft: bottomLeft, bottomRight: bottomRight, topRight: dragPoint.topRightCenter)
//    }
//    
//    func handleDragBottomRight(dragLocation: CGPoint) {
//        var disp: CGSize = self.deriveDisplacementFromDragLocation(dragLocation, draggedPoint: dragPoint.bottomRightCenter, oppositePoint: dragPoint.topLeftCenter)
//        var bottomRight: CGPoint = CGPointMake(dragPoint.bottomRightCenter.x + disp.width, dragPoint.bottomRightCenter.y + disp.height)
//        var topRight: CGPoint = CGPointMake(dragPoint.topRightCenter.x + disp.width, dragPoint.topRightCenter.y)
//        var bottomLeft: CGPoint = CGPointMake(dragPoint.bottomLeftCenter.x, dragPoint.bottomLeftCenter.y + disp.height)
//        // Make sure that the new cropping area will not be smaller than the minimum image size
//        var width: CGFloat = bottomRight.x - bottomLeft.x
//        var height: CGFloat = bottomRight.y - topRight.y
//        width = width * self.imageScale
//        height = height * self.imageScale
//        // If the crop area is too small, set the points at the minimum spacing.
//        var cropArea: CGRect = self.cropAreaFromControlPoints()
//        if width >= IMAGE_MIN_WIDTH && height < IMAGE_MIN_HEIGHT {
//            bottomRight.y = cropArea.origin.y + (IMAGE_MIN_HEIGHT / self.imageScale)
//            bottomLeft.y = bottomRight.y
//        }
//        else if width < IMAGE_MIN_WIDTH && height >= IMAGE_MIN_HEIGHT {
//            bottomRight.x = cropArea.origin.x + (IMAGE_MIN_WIDTH / self.imageScale)
//            topRight.x = bottomRight.x
//        }
//        else if width < IMAGE_MIN_WIDTH && height < IMAGE_MIN_HEIGHT {
//            bottomRight.x = cropArea.origin.x + (IMAGE_MIN_WIDTH / self.imageScale)
//            bottomRight.y = cropArea.origin.y + (IMAGE_MIN_HEIGHT / self.imageScale)
//            topRight.x = bottomRight.x
//            bottomLeft.y = bottomRight.y
//        }
//        
//        self.boundingBoxForTopLeft(dragPoint.topLeftCenter, bottomLeft: bottomLeft, bottomRight: bottomRight, topRight: topRight)
//    }
//    
//    func handleDragTopRight(dragLocation: CGPoint) {
//        var disp: CGSize = self.deriveDisplacementFromDragLocation(dragLocation, draggedPoint: dragPoint.topRightCenter, oppositePoint: dragPoint.bottomLeftCenter)
//        var topRight: CGPoint = CGPointMake(dragPoint.topRightCenter.x + disp.width, dragPoint.topRightCenter.y + disp.height)
//        var topLeft: CGPoint = CGPointMake(dragPoint.topLeftCenter.x, dragPoint.topLeftCenter.y + disp.height)
//        var bottomRight: CGPoint = CGPointMake(dragPoint.bottomRightCenter.x + disp.width, dragPoint.bottomRightCenter.y)
//        // Make sure that the new cropping area will not be smaller than the minimum image size
//        var width: CGFloat = topRight.x - topLeft.x
//        var height: CGFloat = bottomRight.y - topRight.y
//        width = width * self.imageScale
//        height = height * self.imageScale
//        // If the crop area is too small, set the points at the minimum spacing.
//        var cropArea: CGRect = self.cropAreaFromControlPoints()
//        if width >= IMAGE_MIN_WIDTH && height < IMAGE_MIN_HEIGHT {
//            topRight.y = cropArea.origin.y + (((cropArea.size.height * self.imageScale) - IMAGE_MIN_HEIGHT) / self.imageScale)
//            topLeft.y = topRight.y
//        }
//        else if width < IMAGE_MIN_WIDTH && height >= IMAGE_MIN_HEIGHT {
//            topRight.x = cropArea.origin.x + (IMAGE_MIN_WIDTH / self.imageScale)
//            bottomRight.x = topRight.x
//        }
//        else if width < IMAGE_MIN_WIDTH && height < IMAGE_MIN_HEIGHT {
//            topRight.x = cropArea.origin.x + (IMAGE_MIN_WIDTH / self.imageScale)
//            topRight.y = cropArea.origin.y + (((cropArea.size.height * self.imageScale) - IMAGE_MIN_HEIGHT) / self.imageScale)
//            topLeft.y = topRight.y
//            bottomRight.x = topRight.x
//        }
//        
//        self.boundingBoxForTopLeft(topLeft, bottomLeft: dragPoint.bottomLeftCenter, bottomRight: bottomRight, topRight: topRight)
//    }
//    
//    func handleDragCropArea(dragLocation: CGPoint) {
//        var dX: CGFloat = dragLocation.x - dragPoint.dragStart.x
//        var dY: CGFloat = dragLocation.y - dragPoint.dragStart.y
//        var newTopLeft: CGPoint = CGPointMake(dragPoint.topLeftCenter.x + dX, dragPoint.topLeftCenter.y + dY)
//        var newBottomLeft: CGPoint = CGPointMake(dragPoint.bottomLeftCenter.x + dX, dragPoint.bottomLeftCenter.y + dY)
//        var newBottomRight: CGPoint = CGPointMake(dragPoint.bottomRightCenter.x + dX, dragPoint.bottomRightCenter.y + dY)
//        var newTopRight: CGPoint = CGPointMake(dragPoint.topRightCenter.x + dX, dragPoint.topRightCenter.y + dY)
//        var cropAreaWidth: CGFloat = dragPoint.topRightCenter.x - dragPoint.topLeftCenter.x
//        var cropAreaHeight: CGFloat = dragPoint.bottomLeftCenter.y - dragPoint.topLeftCenter.y
//        var minX: CGFloat = imageFrameInView.origin.x
//        var maxX: CGFloat = imageFrameInView.origin.x + imageFrameInView.size.width
//        var minY: CGFloat = imageFrameInView.origin.y
//        var maxY: CGFloat = imageFrameInView.origin.y + imageFrameInView.size.height
//        if newTopLeft.x < minX {
//            newTopLeft.x = minX
//            newBottomLeft.x = minX
//            newTopRight.x = newTopLeft.x + cropAreaWidth
//            newBottomRight.x = newTopRight.x
//        }
//        if newTopLeft.y < minY {
//            newTopLeft.y = minY
//            newTopRight.y = minY
//            newBottomLeft.y = newTopLeft.y + cropAreaHeight
//            newBottomRight.y = newBottomLeft.y
//        }
//        if newBottomRight.x > maxX {
//            newBottomRight.x = maxX
//            newTopRight.x = maxX
//            newTopLeft.x = newBottomRight.x - cropAreaWidth
//            newBottomLeft.x = newTopLeft.x
//        }
//        if newBottomRight.y > maxY {
//            newBottomRight.y = maxY
//            newBottomLeft.y = maxY
//            newTopRight.y = newBottomRight.y - cropAreaHeight
//            newTopLeft.y = newTopRight.y
//        }
//        topLeftPoint.center = newTopLeft
//        bottomLeftPoint.center = newBottomLeft
//        bottomRightPoint.center = newBottomRight
//        topRightPoint.center = newTopRight
//    }
//    
//    func setUserInteractionEnabled(userInteractionEnabled: Bool) {
//        if !userInteractionEnabled {
//            topLeftPoint.hidden = true
//            bottomLeftPoint.hidden = true
//            bottomRightPoint.hidden = true
//            topRightPoint.hidden = true
//        }
//        super.userInteractionEnabled = userInteractionEnabled
//    }
//}
//
//// MARK: ImageCropViewController interface
//
//protocol ImageCropViewControllerDelegate: NSObject {
//    func ImageCropViewControllerSuccess(controller: UIViewController, didFinishCroppingImage croppedImage: UIImage)
//    
//    func ImageCropViewControllerDidCancel(controller: UIViewController)
//}
//class ImageCropViewController: UIViewController, UIActionSheetDelegate {
//    var actionSheet: UIActionSheet
//    
//    weak var delegate: ImageCropViewControllerDelegate
//    var blurredBackground: Bool
//    var image: UIImage {
//        get {
//            return self.image
//        }
//        set(image) {
//            var frameWidth: CGFloat = self.frame.size.width
//            var frameHeight: CGFloat = self.frame.size.height
//            var imageWidth: CGFloat = image.size.width
//            var imageHeight: CGFloat = image.size.height
//            var isPortrait: Bool = imageHeight / frameHeight > imageWidth / frameWidth
//            var x: Int
//            var y: Int
//            var scaledImageWidth: Int
//            var scaledImageHeight: Int
//            if isPortrait {
//                imageScale = imageHeight / frameHeight
//                scaledImageWidth = imageWidth / imageScale
//                scaledImageHeight = frameHeight
//                x = (frameWidth - scaledImageWidth) / 2
//                y = 0
//            }
//            else {
//                imageScale = imageWidth / frameWidth
//                scaledImageWidth = frameWidth
//                scaledImageHeight = imageHeight / imageScale
//                x = 0
//                y = (frameHeight - scaledImageHeight) / 2
//            }
//            imageFrameInView = CGRectMake(x, y, scaledImageWidth, scaledImageHeight)
//            imageView.frame = imageFrameInView
//            imageView.image = image
//            /* prepare imageviews and their frame */
//            self.shadeView.blurredImageView.contentMode = .ScaleAspectFill
//            imageView.contentMode = .ScaleAspectFill
//            imageView.clipsToBounds = true
//            self.shadeView.blurredImageView.clipsToBounds = true
//            var blurFrame: CGRect
//            if imageFrameInView.origin.x < 0 && (imageFrameInView.size.width - fabs(imageFrameInView.origin.x) >= 320) {
//                blurFrame = self.frame
//            }
//            else {
//                blurFrame = imageFrameInView
//            }
//            imageView.frame = imageFrameInView
//            // blurredimageview is on top of shadeview so shadeview needs the same frame as imageView.
//            self.shadeView.frame = imageFrameInView
//            self.shadeView.blurredImageView.frame = blurFrame
//            // perform image blur
//            UIImage * blur
//            if self.blurred {
//                blur = image.blurredImageWithRadius(30, iterations: 1, tintColor: UIColor.blackColor())
//            }
//            else {
//                blur = image.blurredImageWithRadius(0, iterations: 1, tintColor: UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0))
//            }
//            self.shadeView.blurredImageView.image = blur
//            //Special fix. If scaledImageWidth or scaledImageHeight < cropArea.width of cropArea.Height.
//            self.boundingBoxForTopLeft(topLeftPoint.center, bottomLeft: bottomLeftPoint.center, bottomRight: bottomRightPoint.center, topRight: topRightPoint.center)
//            var cropArea: CGRect = self.cropAreaFromControlPoints()
//            cropAreaView.frame = cropArea
//            cropArea.origin.y = cropArea.origin.y - imageFrameInView.origin.y
//            cropArea.origin.x = cropArea.origin.x - imageFrameInView.origin.x
//            self.shadeView.cropArea = cropArea
//        }
//    }
//    
//    var cropView: ImageCropView
//    /**
//     *  the crop area in the image
//     */
//    var cropArea: CGRect {
//        get {
//            if self.cropView {
//                return self.cropView.cropAreaInImage
//            }
//            else {
//                return CGRectZero
//            }
//        }
//        set(cropArea) {
//            self.cropArea = cropArea
//            if self.cropView {
//                self.cropView.cropAreaInImage = cropArea
//            }
//        }
//    }
//    
//    
//    init(image: UIImage) {
//        super.init()
//        if self != nil {
//            self.image = image.fixOrientation()
//        }
//    }
//    
//    @IBAction func cancel(sender: AnyObject) {
//        if self.delegate.respondsToSelector(#selector(self.ImageCropViewControllerDidCancel)) {
//            self.delegate.ImageCropViewControllerDidCancel(self)
//        }
//    }
//    
//    @IBAction func done(sender: AnyObject) {
//        if self.delegate.respondsToSelector(Selector("ImageCropViewControllerSuccess:didFinishCroppingImage:")) {
//            var cropped: UIImage
//            if self.image != nil {
//                var CropRect: CGRect = self.cropView.cropAreaInImage
//                var imageRef: CGImageRef = CGImageCreateWithImageInRect(self.image.CGImage, CropRect)
//                cropped = UIImage.imageWithCGImage(imageRef)
//                CGImageRelease(imageRef)
//            }
//            self.delegate.ImageCropViewControllerSuccess(self, didFinishCroppingImage: cropped)
//        }
//    }
//    
//    
//    override func loadView() {
//        super.loadView()
//    }
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        if self != nil {
//            var contentView: UIView = UIView()
//            contentView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
//            contentView.backgroundColor = UIColor.whiteColor()
//            self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: #selector(self.cancel))
//            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: #selector(self.done))
//            var statusBarSize: CGSize = UIApplication.sharedApplication().statusBarFrame().size
//            var view: CGRect = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - self.navigationController().navigationBar().bounds.size.height - statusBarSize.height)
//            self.cropView = ImageCropView(frame: view, blurOn: self.blurredBackground)
//            self.view = contentView
//            contentView.addSubview(cropView)
//            cropView.image = self.image
//            if cropArea.size.width > 0 {
//                self.cropView.cropAreaInImage = cropArea
//            }
//        }
//    }
//    var cropArea: CGRect
//    
//}
//extension UIImage {
//    func fixOrientation() -> UIImage {
//        // No-op if the orientation is already correct
//        if self.imageOrientation == .Up {
//            
//        }
//        // We need to calculate the proper transformation to make the image upright.
//        // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
//        var transform: CGAffineTransform = CGAffineTransformIdentity
//        switch self.imageOrientation {
//        case .Down, .DownMirrored:
//            transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height)
//            transform = CGAffineTransformRotate(transform, M_PI)
//        case .Left, .LeftMirrored:
//            transform = CGAffineTransformTranslate(transform, self.size.width, 0)
//            transform = CGAffineTransformRotate(transform, M_PI_2)
//        case .Right, .RightMirrored:
//            transform = CGAffineTransformTranslate(transform, 0, self.size.height)
//            transform = CGAffineTransformRotate(transform, -M_PI_2)
//        case .Up, .UpMirrored:
//            break
//        }
//        
//        switch self.imageOrientation {
//        case .UpMirrored, .DownMirrored:
//            transform = CGAffineTransformTranslate(transform, self.size.width, 0)
//            transform = CGAffineTransformScale(transform, -1, 1)
//        case .LeftMirrored, .RightMirrored:
//            transform = CGAffineTransformTranslate(transform, self.size.height, 0)
//            transform = CGAffineTransformScale(transform, -1, 1)
//        case .Up, .Down, .Left, .Right:
//            break
//        }
//        
//        // Now we draw the underlying CGImage into a new context, applying the transform
//        // calculated above.
//        var ctx: CGContextRef = CGBitmapContextCreate(nil, self.size.width, self.size.height, CGImageGetBitsPerComponent(self.CGImage), 0, CGImageGetColorSpace(self.CGImage), CGImageGetBitmapInfo(self.CGImage))
//        CGContextConcatCTM(ctx, transform)
//        switch self.imageOrientation {
//        case .Left, .LeftMirrored, .Right, .RightMirrored:
//            // Grr...
//            CGContextDrawImage(ctx, CGRectMake(0, 0, self.size.height, self.size.width), self.CGImage)
//        default:
//            CGContextDrawImage(ctx, CGRectMake(0, 0, self.size.width, self.size.height), self.CGImage)
//        }
//        
//        // And now we just create a new UIImage from the drawing context
//        var cgimg: CGImageRef = CGBitmapContextCreateImage(ctx)
//        var img: UIImage = UIImage.imageWithCGImage(cgimg)
//        CGContextRelease(ctx)
//        CGImageRelease(cgimg)
//        return img
//    }
//}
////
////  MaskView.m
////
////  Created by Ming Yang on 7/7/12.
////
//let DEFAULT_MASK_ALPHA: CGFloat = 0.75
//
//let square: Bool = false
//
//var IMAGE_MIN_HEIGHT: CGFloat = 400
//
//var IMAGE_MIN_WIDTH: CGFloat = 400
//
//
//// MARK: ImageCropViewController implementation
//
//
//
//// MARK: ControlPointView implementation
//
//
//
//// MARK: - MaskView implementation
//
//
//
//// MARK: - MaskImageView implementation
//
//let DEFAULT_CONTROL_POINT_SIZE: CGFloat = 5
//
//func SquareCGRectAtCenter(centerX: CGFloat, centerY: CGFloat, size: CGFloat) -> CGRect {
//    var x: CGFloat = centerX - size / 2.0
//    var y: CGFloat = centerY - size / 2.0
//    return CGRectMake(x, y, size, size)
//}