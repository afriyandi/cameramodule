    //
    //  MADrawRect.m
    //  instaoverlay
    //
    //  Created by Maximilian Mackh on 11/5/12.
    //  Copyright (c) 2012 mackh ag. All rights reserved.
    //

#import "MADrawRect.h"

@implementation MADrawRect

@synthesize pointD = _pointD;
@synthesize pointC = _pointC;
@synthesize pointB = _pointB;
@synthesize pointA = _pointA;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
        {
        [self setPoints];
        [self setClipsToBounds:NO];
        [self setBackgroundColor:[UIColor clearColor]];
        [self setUserInteractionEnabled:YES];
        [self setContentMode:UIViewContentModeRedraw];
        
        _pointD = [UIButton buttonWithType:UIButtonTypeCustom];
        [_pointD setTag:4];
        [_pointD titleLabel].text = @"point D";
        [_pointD setShowsTouchWhenHighlighted:YES];
        [_pointD addTarget:self action:@selector(pointMoved:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [_pointD addTarget:self action:@selector(pointMoveEnter:withEvent:) forControlEvents:UIControlEventTouchDown];
        [_pointD addTarget:self action:@selector(pointMoveExit:withEvent:) forControlEvents:UIControlEventTouchDragExit];
        [_pointD addTarget:self action:@selector(touchesEnded:withEvent:) forControlEvents:UIControlEventTouchUpInside];
        [_pointD setImage:[self squareButtonWithWidth:50] forState:UIControlStateNormal];
        [self setButtonShape:_pointD];
        [_pointD setAccessibilityLabel:@"Draggable Crop Button Upper Left hand Corner. Double tap & hold to drag and adjust image frame."];
        [self addSubview:_pointD];
        
        _pointC = [UIButton buttonWithType:UIButtonTypeCustom];
        [_pointC setTag:3];
        [_pointC titleLabel].text = @"point C";
        [_pointC setShowsTouchWhenHighlighted:YES];
        [_pointC addTarget:self action:@selector(pointMoved:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [_pointC addTarget:self action:@selector(pointMoveEnter:withEvent:) forControlEvents:UIControlEventTouchDown];
        [_pointC addTarget:self action:@selector(pointMoveExit:withEvent:) forControlEvents:UIControlEventTouchDragExit];
        [_pointC addTarget:self action:@selector(touchesEnded:withEvent:) forControlEvents:UIControlEventTouchUpInside];
        [_pointC setImage:[self squareButtonWithWidth:50] forState:UIControlStateNormal];
        [self setButtonShape:_pointC];
        [_pointC setAccessibilityLabel:@"Draggable Crop Button Upper Right hand Corner."];
        [self addSubview:_pointC];
        
        _pointB = [UIButton buttonWithType:UIButtonTypeCustom];
        [_pointB setTag:2];
        [_pointB titleLabel].text = @"point B";
        [_pointB setShowsTouchWhenHighlighted:YES];
        [_pointB addTarget:self action:@selector(pointMoved:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [_pointB addTarget:self action:@selector(pointMoveEnter:withEvent:) forControlEvents:UIControlEventTouchDown];
        [_pointB addTarget:self action:@selector(pointMoveExit:withEvent:) forControlEvents:UIControlEventTouchDragExit];
        [_pointB addTarget:self action:@selector(touchesEnded:withEvent:) forControlEvents:UIControlEventTouchUpInside];
        [_pointB setImage:[self squareButtonWithWidth:50] forState:UIControlStateNormal];
        [self setButtonShape:_pointB];
        [_pointB setAccessibilityLabel:@"Draggable Crop Button Bottom Right hand Corner."];
        [self addSubview:_pointB];
        
        _pointA = [UIButton buttonWithType:UIButtonTypeCustom];
        [_pointA setTag:1];
        [_pointA titleLabel].text = @"point A";
        [_pointA setShowsTouchWhenHighlighted:YES];
        [_pointA addTarget:self action:@selector(pointMoved:withEvent:) forControlEvents:UIControlEventTouchDragInside];
        [_pointA addTarget:self action:@selector(pointMoveEnter:withEvent:) forControlEvents:UIControlEventTouchDown];
        [_pointA addTarget:self action:@selector(pointMoveExit:withEvent:) forControlEvents:UIControlEventTouchDragExit];
        [_pointA addTarget:self action:@selector(touchesEnded:withEvent:) forControlEvents:UIControlEventTouchUpInside];
        [_pointA setImage:[self squareButtonWithWidth:50] forState:UIControlStateNormal];
        [self setButtonShape:_pointA];
        [_pointA setAccessibilityLabel:@"Draggable Crop Button Bottom Left hand Corner."];
        [self addSubview:_pointA];
        
        [_pointA setShowsTouchWhenHighlighted:NO];
        [_pointB setShowsTouchWhenHighlighted:NO];
        [_pointC setShowsTouchWhenHighlighted:NO];
        [_pointD setShowsTouchWhenHighlighted:NO];
        
        [self setButtons];
        }
    return self;
}

-(void)setButtonShape:(UIButton *)btn{
    CALayer *lyr = [CALayer layer];
    lyr.frame = CGRectMake(btn.frame.origin.x+(250/2)-25, btn.frame.origin.y+(250/2)-25, 50, 50);
    lyr.backgroundColor = [UIColor colorWithRed:0.2 green:0.4 blue:1 alpha:.5f].CGColor;
    lyr.cornerRadius = 25;
    [btn.layer addSublayer:lyr];
}

- (UIImage *)squareButtonWithWidth:(int)width
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(width, width), NO, 0.0);
    UIImage *blank = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return blank;
}

- (void)setPoints
{
    a = CGPointMake(0 + 10, self.bounds.size.height - 10);
    b = CGPointMake(self.bounds.size.width - 10, self.bounds.size.height - 10);
    c = CGPointMake(self.bounds.size.width - 10, 0 + 10);
    d = CGPointMake(0 + 10, 0 + 10);
}

- (void)setButtons
{
    [_pointD setFrame:CGRectMake(d.x - 250 / 2, d.y - 250 / 2, 250, 250)];
    [_pointC setFrame:CGRectMake(c.x - 250 / 2, c.y - 250 / 2, 250, 250)];
    [_pointB setFrame:CGRectMake(b.x - 250 / 2, b.y - 250 / 2, 250, 250)];
    [_pointA setFrame:CGRectMake(a.x - 250 / 2, a.y - 250 / 2, 250, 250)];
}

- (void)bottomLeftCornerToCGPoint: (CGPoint)point
{
    a = point;
    [self needsRedraw];
}

- (void)bottomRightCornerToCGPoint: (CGPoint)point
{
    b = point;
    [self needsRedraw];
}

- (void)topRightCornerToCGPoint: (CGPoint)point
{
    c = point;
    [self needsRedraw];
}

- (void)topLeftCornerToCGPoint: (CGPoint)point
{
    d = point;
    [self needsRedraw];
}

- (void)needsRedraw
{
    frameMoved = YES;
    [self setNeedsDisplay];
    [self setButtons];
    [self drawRect:self.bounds];
}

- (void)drawRect:(CGRect)rect;
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    if (context)
        {
        CGContextSetRGBFillColor(context, 0.0f, 0.0f, 0.0f, 0.7f);
        CGContextSetRGBStrokeColor(context, 1.00f, 0.43f, 0.08f, 1.0f);
        CGContextSetLineJoin(context, kCGLineJoinRound);
        CGContextSetLineWidth(context, 4.0f);
        
        CGRect boundingRect = CGContextGetClipBoundingBox(context);
        CGContextAddRect(context, boundingRect);
        CGContextFillRect(context, boundingRect);
        
        CGMutablePathRef pathRef = CGPathCreateMutable();
        CGPathMoveToPoint(pathRef, NULL, a.x, a.y);
        CGPathAddLineToPoint(pathRef, NULL, b.x, b.y);
        CGPathAddLineToPoint(pathRef, NULL, c.x, c.y);
        CGPathAddLineToPoint(pathRef, NULL, d.x, d.y);
        CGPathCloseSubpath(pathRef);
        
        CGContextAddPath(context, pathRef);
        CGContextStrokePath(context);
        
        CGContextSetBlendMode(context, kCGBlendModeClear);
        
        CGContextAddPath(context, pathRef);
        CGContextFillPath(context);
        
        CGContextSetBlendMode(context, kCGBlendModeNormal);
        
        CGPathRelease(pathRef);
        }
}

- (IBAction) pointMoveEnter:(id) sender withEvent:(UIEvent *) event
{
    UIControl *control = sender;
    CGPoint raw = [[[event allTouches] anyObject] locationInView:self];
    touchOffset = CGPointMake( raw.x - control.center.x, raw.y - control.center.y);
    /*if(mgGlass == nil){
     mgGlass = [[APMagnifier alloc] init];
     mgGlass.magGlass = self;
     }
     mgGlass.tchPt = raw;
     [self addSubview:mgGlass];
     [mgGlass setNeedsDisplay];*/
}

- (IBAction) pointMoved:(id) sender withEvent:(UIEvent *) event
{
    CGPoint point = [[[event allTouches] anyObject] locationInView:self];
    point = CGPointMake(point.x - touchOffset.x, point.y - touchOffset.y);
    
    if (CGRectContainsPoint(self.bounds, point))
        {
        frameMoved = YES;
        UIControl *control = sender;
        control.center = point;
        
        switch (control.tag) {
            case 1:
                a = point;
                break;
            case 2:
                b = point;
                break;
            case 3:
                c = point;
                break;
            case 4:
                d = point;
                break;
        }
        [self setNeedsDisplay];
        [self drawRect:self.bounds];
        [self handleAction:point];
        }
    else
        {
        float kLineOffsetWidth = 2.0f;
        
        if (point.x < kLineOffsetWidth || point.x > self.bounds.size.width - kLineOffsetWidth)
            {
            if (point.x < kLineOffsetWidth)
                {
                point.x = kLineOffsetWidth;
                }
            else if (point.x > self.bounds.size.width - kLineOffsetWidth)
                {
                point.x = self.bounds.size.width - kLineOffsetWidth;
                }
            }
        
        if (point.y < kLineOffsetWidth || point.y > self.bounds.size.height - kLineOffsetWidth)
            {
            if (point.y < kLineOffsetWidth)
                {
                point.y = kLineOffsetWidth;
                }
            else if (point.y > self.bounds.size.height)
                {
                point.y = self.bounds.size.height - kLineOffsetWidth;
                }
            }
        
        frameMoved = YES;
        UIControl *control = sender;
        control.center = point;
        
        
        switch (control.tag) {
            case 1:
                a = point;
                break;
            case 2:
                b = point;
                break;
            case 3:
                c = point;
                break;
            case 4:
                d = point;
                break;
        }
        [self setNeedsDisplay];
        [self drawRect:self.bounds];
        [self handleAction:point];
        }
}

- (IBAction) pointMoveExit:(id) sender withEvent:(UIEvent *) event
{
    touchOffset = CGPointZero;
        //[mgGlass removeFromSuperview];
}

- (void) resetFrame
{
    [self setPoints];
    [self setNeedsDisplay];
    [self drawRect:self.bounds];
    frameMoved = NO;
    [self setButtons];
}

- (BOOL) frameEdited
{
    return frameMoved;
}

- (CGPoint)coordinatesForPoint: (int)point withScaleFactor: (CGFloat)scaleFactor
{
    CGPoint tmp = CGPointMake(0, 0);
    
    switch (point) {
        case 1:
            tmp = CGPointMake(a.x / scaleFactor, a.y / scaleFactor);
            break;
        case 2:
            tmp = CGPointMake(b.x / scaleFactor, b.y / scaleFactor);
            break;
        case 3:
            tmp = CGPointMake(c.x / scaleFactor, c.y / scaleFactor);
            break;
        case 4:
            tmp =  CGPointMake(d.x / scaleFactor, d.y / scaleFactor);
            break;
    }
    
    return tmp;
}

- (CGPoint)coordinatesForPoint: (int)point withScaleFactorx: (CGFloat)scaleFactorx withScaleFactory: (CGFloat)scaleFactory
{
    CGPoint tmp = CGPointMake(0, 0);
    
    switch (point) {
        case 1:
            tmp = CGPointMake(a.x / scaleFactorx, a.y / scaleFactory);
            break;
        case 2:
            tmp = CGPointMake(b.x / scaleFactorx, b.y / scaleFactory);
            break;
        case 3:
            tmp = CGPointMake(c.x / scaleFactorx, c.y / scaleFactory);
            break;
        case 4:
            tmp =  CGPointMake(d.x / scaleFactorx, d.y / scaleFactory);
            break;
    }
    
    return tmp;
}

    //magnifying glass

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
        //[mgGlass removeFromSuperview];
}

- (void)handleAction:(CGPoint) pt {
        //mgGlass.tchPt = pt;
        //[mgGlass setNeedsDisplay];
}
    //end magnifying glass


@end

