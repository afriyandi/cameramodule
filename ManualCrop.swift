//
//  ManualCrop.swift
//  O2OcameraModule
//
//  Created by Afriyandi Setiawan on 8/3/16.
//  Copyright © 2016 Afriyandi Setiawan. All rights reserved.
//

import UIKit

class ManualCrop: UIViewController {
    
    var coordinate:CIRectangleFeature!
    var originalImage:UIImage!
    var layoutLine:MADrawRect!
    @IBOutlet weak var outletImage: UIImageView!
    @IBOutlet weak var backButt: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        outletImage.image = originalImage
        print("outlet frame: \(originalImage.size)")
        print("the View size: \(self.view.frame)")
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        dispatch_async(dispatch_get_main_queue()) {
            guard let coordinate = self.coordinate else {
                return
            }
            let scaleW = self.view.frame.width/self.originalImage.size.width
            let scaleH = self.view.frame.height/self.originalImage.size.height
            let centerPoint = CGPointMake(CGRectGetMidX(self.view.frame), CGRectGetMidY(self.view.frame))
            
            
            self.layoutLine = MADrawRect(frame: self.view.frame)
            let btXLeft = (coordinate.bottomLeft.x*scaleW)
            let btYLeft = (coordinate.bottomLeft.y*scaleH)
            let btXRight = (coordinate.bottomRight.x*scaleW)
            let btYRight = (coordinate.bottomRight.y*scaleH)
            let tpXLeft = (coordinate.topLeft.x*scaleW)
            let tpYLeft = (coordinate.topLeft.y*scaleH)
            let tpXRight = (coordinate.topRight.x*scaleW)
            let tpYRight = (coordinate.topRight.y*scaleH)
            
            let pseudoTL = CGPointMake(tpXLeft, tpYLeft)
            let xAxisTL = CGPointMake((centerPoint.x - pseudoTL.x), (centerPoint.y - pseudoTL.y))
            let pseudoTR = CGPointMake(tpXRight, tpYRight)
            let xAxisTR = CGPointMake((centerPoint.x - pseudoTR.x), (centerPoint.y - pseudoTR.y))
            let pseudoBL = CGPointMake(btXLeft, btYLeft)
            let xAxisBL = CGPointMake((centerPoint.x - pseudoBL.x), (centerPoint.y - pseudoBL.y))
            let pseudoBR = CGPointMake(btXRight, btYRight)
            let xAxisBR = CGPointMake((centerPoint.x - pseudoBR.x), (centerPoint.y - pseudoBR.y))
            let TL = CGPointMake(pseudoTL.x, (centerPoint.y + xAxisTL.y))
            let TR = CGPointMake(pseudoTR.x, (centerPoint.y + xAxisTR.y))
            let BL = CGPointMake(pseudoBL.x, (centerPoint.y + xAxisBL.y))
            let BR = CGPointMake(pseudoBR.x, (centerPoint.y + xAxisBR.y))
            
            self.layoutLine.topLeftCornerToCGPoint(TL)
            self.layoutLine.topRightCornerToCGPoint(TR)
            self.layoutLine.bottomLeftCornerToCGPoint(BL)
            self.layoutLine.bottomRightCornerToCGPoint(BR)
            self.view.addSubview(self.layoutLine)
            self.view.bringSubviewToFront(self.backButt)
        }
    }

}
