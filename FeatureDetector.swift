//
//  VideoFilter.swift
//  O2OcameraModule
//
//  Created by Afriyandi Setiawan on 6/27/16.
//  Copyright © 2016 Afriyandi Setiawan. All rights reserved.
//

protocol Counter {
    var varCount: Double {get set}
    var stabilCount: Double {get set}
    
    init()
}

protocol CounterWorker {
    func add<T: Counter>(caller: T) -> T
}

extension Counter {
    func count(worker: CounterWorker) -> Self {
        return worker.add(self)
    }
}

struct CounterAdapter: Counter {
    var varCount: Double = 0
    var stabilCount: Double = 0
}

struct FeatureDetector {
    var image:CIImage
    let mode:DetectMode
    
    init(mode: DetectMode?, image: CIImage) {
        self.mode = mode!
        self.image = image
    }
    
    func prepareRectDetector() -> CIDetector {
        let options: [String: AnyObject] = [CIDetectorAccuracy: CIDetectorAccuracyHigh, CIDetectorMinFeatureSize: 0.7]
        return CIDetector(ofType: CIDetectorTypeRectangle, context: nil, options: options)
    }
    
    func doRectDetect(needCoordinate: Bool = false) -> (rectangle: AnyObject?, isDetected: Bool) {
        let detectionMethod = self.prepareRectDetector()
        let gimage = self.image.imageByApplyingFilter("CIColorPosterize", withInputParameters: ["inputImage": self.image, "inputLevels": 2.0])
        let features = detectionMethod.featuresInImage(gimage, options: [CIDetectorAspectRatio: 1.414])
        var biggestRect:CIRectangleFeature!
        
        if features.count == 0 {
            return (self.image, false)
        }
        
        var halfPerimiterValue:Float = 0
        for feature in features as! [CIRectangleFeature] {
            let p1:CGPoint = feature.topLeft
            let p2:CGPoint = feature.topRight
            let width:Float = hypotf(Float(p1.x) - Float(p2.x), Float(p1.y) - Float(p2.y))
            let p3:CGPoint = feature.topLeft
            let p4:CGPoint = feature.bottomLeft
            let height:Float = hypotf(Float(p3.x) - Float(p4.x), Float(p3.y) - Float(p4.y))
            let currentHalfPerimeterValue:Float = height + width
            
            if (halfPerimiterValue < currentHalfPerimeterValue) {
                halfPerimiterValue = currentHalfPerimeterValue
                biggestRect = feature
            }
        }
        
        if needCoordinate {
            return (biggestRect, true)
        }
        
        return (drawThePoint(biggestRect), true)
    }
    
    func drawThePoint(feature: CIFeature) -> CIImage {
        var overlay_ = CIImage(color: CIColor(red: 0.0, green: 0, blue: 1.0, alpha: 0.3))
        overlay_ = overlay_.imageByCroppingToRect(self.image.extent)
        let rectFeature = feature as! CIRectangleFeature
        overlay_ = overlay_.imageByApplyingFilter("CIPerspectiveTransformWithExtent",
                                                  withInputParameters: [
                                                    "inputExtent": CIVector(CGRect: self.image.extent),
                                                    "inputTopLeft": CIVector(CGPoint: rectFeature.topLeft),
                                                    "inputTopRight": CIVector(CGPoint: rectFeature.topRight),
                                                    "inputBottomLeft": CIVector(CGPoint: rectFeature.bottomLeft),
                                                    "inputBottomRight": CIVector(CGPoint: rectFeature.bottomRight)
            ])
        
        return overlay_.imageByCompositingOverImage(self.image)
    }
    
    func prepareQRCodeDetector() -> CIDetector {
        let options = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        return CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: options)
    }
    
    func doQRDetect() -> (rectangle: AnyObject?, isDetected: Bool) {
        let detectionMethod = self.prepareQRCodeDetector()
        let qrdetected = detectionMethod.featuresInImage(self.image)
        
        for qritem in qrdetected as! [CIQRCodeFeature] {
            print(qritem.messageString)
            return (qritem.messageString, true)
        }
        
        return (self.image, false)
        
    }
    
    func doDetect() -> (rectangle: AnyObject?, isDetected: Bool) {
        switch self.mode {
        case .Detect:
            return doRectDetect()
        case .QR:
            return doQRDetect()
        default:
            return doRectDetect(true)
        }
    }
}
