//
//  PreviewImage.swift
//  O2OcameraModule
//
//  Created by Afriyandi Setiawan on 7/19/16.
//  Copyright © 2016 Afriyandi Setiawan. All rights reserved.
//

class PreviewImage: UIViewController {

    @IBOutlet weak var previewDisplay: UIImageView!
    @IBOutlet var theButton: [UIButton]!
    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var uploadPict: UIButton!
    var comeThrough:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
    var delegate: O2OCamViewCloseDelegate?
    
    override func viewDidLoad() {
        defer {
//            (self.view as! UIScrollView).contentSize = previewDisplay.frame.size
        }
        guard let croppedImage = comeThrough["cropped"] as? UIImage else {
            previewDisplay.image = comeThrough["ori"] as? UIImage
            return
        }
        previewDisplay.image = croppedImage
        previewDisplay.frame = CGRectMake(previewDisplay.frame.origin.x , previewDisplay.frame.origin.y, croppedImage.size.width, croppedImage.size.height)

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        back.roundCorners([.TopRight, .BottomRight, .BottomLeft, .TopLeft], radius: 25)
    }
    
    @IBAction func onDoubleTap(sender: AnyObject) {
        if (self.view as! UIScrollView).zoomScale != 1.0 {
            (self.view as! UIScrollView).setZoomScale(1.0, animated: true)
        } else {
            let zoomScale = min(3.0, (self.view as! UIScrollView).maximumZoomScale)
            let point = (sender as! UIGestureRecognizer).locationInView(self.view)
            let size = (self.view as! UIScrollView).bounds.size
            let w = size.width/zoomScale
            let h = size.height/zoomScale
            let x = point.x - (w/2)
            let y = point.y - (h/2)
            (self.view as! UIScrollView).zoomToRect(CGRectMake(x, y, w, h), animated: true)
            (self.view as! UIScrollView).zoomScale = zoomScale
        }
    }
    
    @IBAction func uploadAndClose(sender: AnyObject) {
        delegate?.closeAndUpload(self.previewDisplay.image!)
    }
    
}

extension PreviewImage: UIScrollViewDelegate {
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.previewDisplay
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        for button in theButton {button.hidden = true}
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if scrollView.zoomScale == 1 {
            for button in theButton {button.hidden = false}
        }
    }
    
    func scrollViewDidEndZooming(scrollView: UIScrollView, withView view: UIView?, atScale scale: CGFloat) {
        if scale == 1 {
            for button in theButton {button.hidden = false}
        }
    }
}