//
//  MADrawRect.h
//  instaoverlay
//
//  Created by Maximilian Mackh on 11/5/12.
//  Copyright (c) 2012 mackh ag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MADrawRect : UIView
{
    CGPoint touchOffset;
    CGPoint a;
    CGPoint b;
    CGPoint c;
    CGPoint d;
    //APMagnifier *mgGlass;
    BOOL frameMoved;
}

@property (strong, nonatomic) UIButton *pointD;
@property (strong, nonatomic) UIButton *pointC;
@property (strong, nonatomic) UIButton *pointB;
@property (strong, nonatomic) UIButton *pointA;

- (BOOL)frameEdited;
- (void)resetFrame;
- (CGPoint)coordinatesForPoint: (int)point withScaleFactor: (CGFloat)scaleFactor;
- (CGPoint)coordinatesForPoint: (int)point withScaleFactorx: (CGFloat)scaleFactorx withScaleFactory: (CGFloat)scaleFactory;

- (void)bottomLeftCornerToCGPoint: (CGPoint)point;
- (void)bottomRightCornerToCGPoint: (CGPoint)point;
- (void)topRightCornerToCGPoint: (CGPoint)point;
- (void)topLeftCornerToCGPoint: (CGPoint)point;

@end
