//
//  ManualCroping.swift
//  O2OcameraModule
//
//  Created by Afriyandi Setiawan on 7/21/16.
//  Copyright © 2016 Afriyandi Setiawan. All rights reserved.
//

class ManualCroping: UIView {
    
    var touchOffset: CGPoint!
    var a: CGPoint = CGPointZero
    var b: CGPoint = CGPointZero
    var c: CGPoint = CGPointZero
    var d: CGPoint = CGPointZero
    //APMagnifier *mgGlass;
    var frameMoved: Bool = false
    var contextSize:CGSize!
    
    var pointD: UIButton = UIButton()
    var pointC: UIButton = UIButton()
    var pointB: UIButton = UIButton()
    var pointA: UIButton = UIButton()
    
    let kCropButtonSize:Float = 250
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setPoints()
        self.clipsToBounds = false
        self.backgroundColor = UIColor.clearColor()
        self.userInteractionEnabled = true
        self.contentMode = .Redraw
        self.pointD = UIButton(type: .Custom)
        pointD.tag = 4
        pointD.showsTouchWhenHighlighted = true
        pointD.addTarget(self, action: #selector(ManualCroping.pointMoved(_:withEvent:)), forControlEvents: .TouchDragInside)
        pointD.addTarget(self, action: #selector(ManualCroping.pointMoveEnter(_:withEvent:)), forControlEvents: .TouchDown)
        pointD.addTarget(self, action: #selector(ManualCroping.pointMoveExit(_:withEvent:)), forControlEvents: .TouchDragExit)
        pointD.addTarget(self, action: #selector(UIResponder.touchesEnded(_:withEvent:)), forControlEvents: .TouchUpInside)
        pointD.setImage(self.squareButtonWithWidth(50), forState: .Normal)
        self.setButtonShape(pointD)
        pointD.accessibilityLabel = "Draggable Crop Button Upper Left hand Corner. Double tap & hold to drag and adjust image frame."
        self.addSubview(pointD)
        self.pointC = UIButton(type: .Custom)
        pointC.tag = 3
        pointC.showsTouchWhenHighlighted = true
        pointC.addTarget(self, action: #selector(ManualCroping.pointMoved(_:withEvent:)), forControlEvents: .TouchDragInside)
        pointC.addTarget(self, action: #selector(ManualCroping.pointMoveEnter(_:withEvent:)), forControlEvents: .TouchDown)
        pointC.addTarget(self, action: #selector(ManualCroping.pointMoveExit(_:withEvent:)), forControlEvents: .TouchDragExit)
        pointC.addTarget(self, action: #selector(UIResponder.touchesEnded(_:withEvent:)), forControlEvents: .TouchUpInside)
        pointC.setImage(self.squareButtonWithWidth(50), forState: .Normal)
        self.setButtonShape(pointC)
        pointC.accessibilityLabel = "Draggable Crop Button Upper Right hand Corner."
        self.addSubview(pointC)
        self.pointB = UIButton(type: .Custom)
        pointB.tag = 2
        pointB.showsTouchWhenHighlighted = true
        pointB.addTarget(self, action: #selector(ManualCroping.pointMoved(_:withEvent:)), forControlEvents: .TouchDragInside)
        pointB.addTarget(self, action: #selector(ManualCroping.pointMoveEnter(_:withEvent:)), forControlEvents: .TouchDown)
        pointB.addTarget(self, action: #selector(ManualCroping.pointMoveExit(_:withEvent:)), forControlEvents: .TouchDragExit)
        pointB.addTarget(self, action: #selector(UIResponder.touchesEnded(_:withEvent:)), forControlEvents: .TouchUpInside)
        pointB.setImage(self.squareButtonWithWidth(50), forState: .Normal)
        self.setButtonShape(pointB)
        pointB.accessibilityLabel = "Draggable Crop Button Bottom Right hand Corner."
        self.addSubview(pointB)
        self.pointA = UIButton(type: .Custom)
        pointA.tag = 1
        pointA.showsTouchWhenHighlighted = true
        pointA.addTarget(self, action: #selector(ManualCroping.pointMoved(_:withEvent:)), forControlEvents: .TouchDragInside)
        pointA.addTarget(self, action: #selector(ManualCroping.pointMoveEnter(_:withEvent:)), forControlEvents: .TouchDown)
        pointA.addTarget(self, action: #selector(ManualCroping.pointMoveExit(_:withEvent:)), forControlEvents: .TouchDragExit)
        pointA.addTarget(self, action: #selector(UIResponder.touchesEnded(_:withEvent:)), forControlEvents: .TouchUpInside)
        pointA.setImage(self.squareButtonWithWidth(50), forState: .Normal)
        self.setButtonShape(pointA)
        pointA.accessibilityLabel = "Draggable Crop Button Bottom Left hand Corner."
        self.addSubview(pointA)
        pointA.showsTouchWhenHighlighted = false
        pointB.showsTouchWhenHighlighted = false
        pointC.showsTouchWhenHighlighted = false
        pointD.showsTouchWhenHighlighted = false
        self.setButtons()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setButtonShape(btn: UIButton) {
        let lyr: CALayer = CALayer()
        lyr.frame = CGRectMake(btn.frame.origin.x + (CGFloat(kCropButtonSize) / 2) - 25, btn.frame.origin.y + (CGFloat(kCropButtonSize) / 2) - 25, 50, 50)
        lyr.backgroundColor = UIColor(red: 0.2, green: 0.4, blue: 1, alpha: 0.5).CGColor
        lyr.cornerRadius = 25
        btn.layer.addSublayer(lyr)
    }
    
    func squareButtonWithWidth(width: Float) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(CGFloat(width), CGFloat(width)), false, 0.0)
        let blank: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return blank
    }
    
    func setPoints() {
        a = CGPointMake(0 + 10, self.bounds.size.height - 10)
        b = CGPointMake(self.bounds.size.width - 10, self.bounds.size.height - 10)
        c = CGPointMake(self.bounds.size.width - 10, 0 + 10)
        d = CGPointMake(0 + 10, 0 + 10)
    }
    
    func setButtons() {
        pointD.frame = CGRectMake(d.x - CGFloat(kCropButtonSize) / 2, d.y - CGFloat(kCropButtonSize) / 2, CGFloat(kCropButtonSize), CGFloat(kCropButtonSize))
        pointC.frame = CGRectMake(c.x - CGFloat(kCropButtonSize) / 2, c.y - CGFloat(kCropButtonSize) / 2, CGFloat(kCropButtonSize), CGFloat(kCropButtonSize))
        pointB.frame = CGRectMake(b.x - CGFloat(kCropButtonSize) / 2, b.y - CGFloat(kCropButtonSize) / 2, CGFloat(kCropButtonSize), CGFloat(kCropButtonSize))
        pointA.frame = CGRectMake(a.x - CGFloat(kCropButtonSize) / 2, a.y - CGFloat(kCropButtonSize) / 2, CGFloat(kCropButtonSize), CGFloat(kCropButtonSize))
    }
    
    
    func bottomLeftCornerToCGPoint(point: CGPoint) {
        a = point
        self.needsRedraw()
    }
    
    func bottomRightCornerToCGPoint(point: CGPoint) {
        b = point
        self.needsRedraw()
    }
    
    func topRightCornerToCGPoint(point: CGPoint) {
        c = point
        self.needsRedraw()
    }
    
    func topLeftCornerToCGPoint(point: CGPoint) {
        d = point
        self.needsRedraw()
    }
    
    func needsRedraw() {
//        dispatch_async(dispatch_get_main_queue()) {
            self.frameMoved = true
            self.setNeedsDisplay()
            self.setButtons()
            self.drawRect(self.bounds)
//        }
    }
    
    override func drawRect(rect: CGRect) {
        UIGraphicsBeginImageContextWithOptions(contextSize, true, 0.0)
        let context: CGContextRef = UIGraphicsGetCurrentContext()!
        CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 0.7)
        CGContextSetRGBStrokeColor(context, 1.00, 0.43, 0.08, 1.0)
        CGContextSetLineJoin(context, CGLineJoin.Round)
        CGContextSetLineWidth(context, 4.0)
        let boundingRect: CGRect = CGContextGetClipBoundingBox(context)
        CGContextAddRect(context, boundingRect)
        CGContextFillRect(context, boundingRect)
        let pathRef: CGMutablePathRef = CGPathCreateMutable()
        CGPathMoveToPoint(pathRef, nil, self.a.x, self.a.y)
        CGPathAddLineToPoint(pathRef, nil, self.b.x, self.b.y)
        CGPathAddLineToPoint(pathRef, nil, self.c.x, self.c.y)
        CGPathAddLineToPoint(pathRef, nil, self.d.x, self.d.y)
        CGPathCloseSubpath(pathRef)
        CGContextAddPath(context, pathRef)
        CGContextStrokePath(context)
        CGContextSetBlendMode(context, CGBlendMode.Clear)
        CGContextAddPath(context, pathRef)
        CGContextFillPath(context)
        CGContextSetBlendMode(context, CGBlendMode.Normal)
        UIGraphicsEndImageContext()
    }
    
    
    @IBAction func pointMoveEnter(sender: AnyObject, withEvent event: UIEvent) {
        let control: UIControl = sender as! UIControl
        let raw: CGPoint = event.allTouches()!.first!.locationInView(self)
        touchOffset = CGPointMake(raw.x - control.center.x, raw.y - control.center.y)
        /*if(mgGlass == nil){
         mgGlass = [[APMagnifier alloc] init];
         mgGlass.magGlass = self;
         }
         mgGlass.tchPt = raw;
         [self addSubview:mgGlass];
         [mgGlass setNeedsDisplay];*/
        /*if(mgGlass == nil){
         mgGlass = [[APMagnifier alloc] init];
         mgGlass.magGlass = self;
         }
         mgGlass.tchPt = raw;
         [self addSubview:mgGlass];
         [mgGlass setNeedsDisplay];*/
    }
    
    @IBAction func pointMoved(sender: AnyObject, withEvent event: UIEvent) {
        var point: CGPoint = event.allTouches()!.first!.locationInView(self)
        point = CGPointMake(point.x - touchOffset.x, point.y - touchOffset.y)
        if CGRectContainsPoint(self.bounds, point) {
            frameMoved = true
            let control: UIControl = sender as! UIControl
            control.center = point
            switch control.tag {
            case 1:
                a = point
            case 2:
                b = point
            case 3:
                c = point
            case 4:
                d = point
            default:
                break
            }
            
            self.setNeedsDisplay()
            self.drawRect(self.bounds)
            self.handleAction(point)
        }
        else {
            let kLineOffsetWidth: Float = 2.0
            if point.x < CGFloat(kLineOffsetWidth) || point.x > self.bounds.size.width - CGFloat(kLineOffsetWidth) {
                if point.x < CGFloat(kLineOffsetWidth) {
                    point.x = CGFloat(kLineOffsetWidth)
                }
                else if point.x > self.bounds.size.width - CGFloat(kLineOffsetWidth) {
                    point.x = self.bounds.size.width - CGFloat(kLineOffsetWidth)
                }
            }
            if point.y < CGFloat(kLineOffsetWidth) || point.y > self.bounds.size.height - CGFloat(kLineOffsetWidth) {
                if point.y < CGFloat(kLineOffsetWidth) {
                    point.y = CGFloat(kLineOffsetWidth)
                }
                else if point.y > self.bounds.size.height {
                    point.y = self.bounds.size.height - CGFloat(kLineOffsetWidth)
                }
            }
            frameMoved = true
            let control: UIControl = sender as! UIControl
            control.center = point
            switch control.tag {
            case 1:
                a = point
            case 2:
                b = point
            case 3:
                c = point
            case 4:
                d = point
            default: break
            }
            
            self.setNeedsDisplay()
            self.drawRect(self.bounds)
            self.handleAction(point)
        }
    }
    
    
    @IBAction func pointMoveExit(sender: AnyObject, withEvent event: UIEvent) {
        touchOffset = CGPointZero
        //[mgGlass removeFromSuperview];
        //[mgGlass removeFromSuperview];
    }
    
    func resetFrame() {
        self.setPoints()
        self.setNeedsDisplay()
        self.drawRect(self.bounds)
        frameMoved = false
        self.setButtons()
    }
    
    func frameEdited() -> Bool {
        return frameMoved
    }
    
    func coordinatesForPoint(point: Int, withScaleFactor scaleFactor: CGFloat) -> CGPoint {
        var tmp: CGPoint = CGPointMake(0, 0)
        switch point {
        case 1:
            tmp = CGPointMake(a.x / scaleFactor, a.y / scaleFactor)
        case 2:
            tmp = CGPointMake(b.x / scaleFactor, b.y / scaleFactor)
        case 3:
            tmp = CGPointMake(c.x / scaleFactor, c.y / scaleFactor)
        case 4:
            tmp = CGPointMake(d.x / scaleFactor, d.y / scaleFactor)
        default: break
        }
        
        return tmp
    }
    
    func coordinatesForPoint(point: Int, withScaleFactorx scaleFactorx: CGFloat, withScaleFactory scaleFactory: CGFloat) -> CGPoint {
        var tmp: CGPoint = CGPointMake(0, 0)
        switch point {
        case 1:
            tmp = CGPointMake(a.x / scaleFactorx, a.y / scaleFactory)
        case 2:
            tmp = CGPointMake(b.x / scaleFactorx, b.y / scaleFactory)
        case 3:
            tmp = CGPointMake(c.x / scaleFactorx, c.y / scaleFactory)
        case 4:
            tmp = CGPointMake(d.x / scaleFactorx, d.y / scaleFactory)
        default: break
        }
        
        return tmp
    }
    //magnifying glass
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //[mgGlass removeFromSuperview];
        //[mgGlass removeFromSuperview];
    }
    
    func handleAction(pt: CGPoint) {
        //mgGlass.tchPt = pt;
        //[mgGlass setNeedsDisplay];
        //mgGlass.tchPt = pt;
        //[mgGlass setNeedsDisplay];
    }
}
