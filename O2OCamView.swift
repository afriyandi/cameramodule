//
//  CamView.swift
//  O2OcameraModule
//
//  Created by Afriyandi Setiawan on 6/28/16.
//  Copyright © 2016 Afriyandi Setiawan. All rights reserved.
//

import UIKit
import GLKit
import AVFoundation

protocol O2OCamViewCloseDelegate {
    func closeAndUpload(image:UIImage) -> ()
}

public protocol O2OCamViewDelegate: class {
    func sendImage(image:UIImage) -> ()
}

public class O2OCamView: UIViewController {
    
    var camSession: AVCaptureSession!
    var sessionQueue: dispatch_queue_t!
    var renderContext:CIContext!
    var videoFeed:GLKView!
    public var mode:DetectMode = .Detect
    dynamic var countVal:Int = 0
    var countContext = 79
    var isCapturing = false
    var passThrough:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
    @IBOutlet weak var captMode: UISegmentedControl!
    @IBOutlet weak var captButton: UIButton!
    public var delegate:O2OCamViewDelegate!
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if captMode.selectedSegmentIndex == 0 {
            captButton.hidden = true
        }
        UIApplication.sharedApplication().idleTimerDisabled = true
        if mode == .Detect {
            self.addObserver(self, forKeyPath: "countVal", options: .New, context: &countContext)
        } else {
            captMode.hidden = true
            captButton.hidden = true
        }
        prepareGLK()
    }
    
    override public func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override public func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        UIApplication.sharedApplication().idleTimerDisabled = false
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override public func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    
    override public func shouldAutorotate() -> Bool {
        return true
    }
    
    @IBAction func unwindToMain(segue: UIStoryboardSegue) {
        self.countVal = 0
        self.isCapturing = false
        self.passThrough = Dictionary<String, AnyObject>()
        startSession()
    }
    
    @IBAction func manualCapt(sender: AnyObject) {
        self.isCapturing = true
        doTheCrop(isManual: true, isDone: { (isDone) in
            if isDone {
                self.performSegueWithIdentifier("gotoManCrop", sender: sender)
            }
        })
    }
    
    @IBAction func segmentChange(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 1 {
            self.captButton.hidden = false
            self.countVal = 0
        } else {
            self.captButton.hidden = true
        }
    }
    
    @IBAction func swipeSegmentChange(sender: UISwipeGestureRecognizer) {
        guard captMode.selectedSegmentIndex == 0 && sender.direction == UISwipeGestureRecognizerDirection.Left else {
            guard captMode.selectedSegmentIndex == 1 && sender.direction == UISwipeGestureRecognizerDirection.Right else {
                return
            }
            captMode.selectedSegmentIndex = 0
            segmentChange(captMode)
            return
        }
        captMode.selectedSegmentIndex = 1
        segmentChange(captMode)
    }
    
    public override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        switch context {
        case &countContext:
            if !self.isCapturing {
                //about 1 seconds waiting
                if countVal > 30 {
                    isCapturing = true
                    doTheCrop(isDone: { (isDone) in
                        if isDone {
                            self.performSegueWithIdentifier("gotoPreview", sender: self)
                        }
                    })
                }
            }
        default: break
        }
    }
    
    override public func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let segueID = segue.identifier else {
            return
        }        
        switch segueID {
        case "gotoPreview":
            let dest = segue.destinationViewController as! PreviewImage
            dest.comeThrough = passThrough
            dest.delegate = self
        case "gotoManCrop":
            guard let tempCoor = self.passThrough["coordinat"] as? CIRectangleFeature else {
                (segue.destinationViewController as! ManualCrop).originalImage = self.passThrough["ori"] as! UIImage
                return
            }
            let dest = segue.destinationViewController as! ManualCrop
            dest.coordinate = tempCoor
            dest.originalImage = self.passThrough["ori"] as! UIImage
        default:
            break
        }
    }
    
    func prepareGLK() {
        videoFeed = GLKView(frame: self.view.bounds, context: EAGLContext(API: .OpenGLES2))
        videoFeed.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        videoFeed.translatesAutoresizingMaskIntoConstraints = true
        videoFeed.contentScaleFactor = 1.0
        self.view.addSubview(videoFeed)
        self.view.sendSubviewToBack(videoFeed)
        renderContext  = CIContext(EAGLContext: videoFeed.context)
        sessionQueue = dispatch_queue_create("O2OCamSession", DISPATCH_QUEUE_SERIAL)
        videoFeed.bindDrawable()
        startSession()
    }
    
    func createSession() -> AVCaptureSession {
        let cam = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        var input:AVCaptureInput
        do {
            input = try AVCaptureDeviceInput(device: cam)
        } catch _ as NSError {
            print("Cannot Init Cam")
            exit(EXIT_FAILURE)
        }
        
        //output
        let videoOut = AVCaptureVideoDataOutput()
        let stillImgOut = AVCaptureStillImageOutput()
        stillImgOut.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]
        stillImgOut.highResolutionStillImageOutputEnabled  = true
        
        videoOut.videoSettings = nil
        videoOut.alwaysDiscardsLateVideoFrames = true
        videoOut.setSampleBufferDelegate(self, queue: sessionQueue)
        
        dispatch_async(dispatch_get_main_queue()) {
            let connection = videoOut.connectionWithMediaType(AVMediaTypeVideo)
            if connection.supportsVideoOrientation {
                connection.videoOrientation = .Portrait
            }
        }
        
        let session = AVCaptureSession()
        //make sure the stream quality good enough.
        session.sessionPreset = AVCaptureSessionPresetPhoto
        
        session.addInput(input)
        session.addOutput(videoOut)
        session.addOutput(stillImgOut)
        
        session.commitConfiguration()
        
        return session
        
    }
    
    func startSession() {
        if camSession == nil {
            camSession = createSession()
        }
        camSession.startRunning()
    }
    
    func finishSession() {
        camSession.stopRunning()
    }
    
    func doTheCrop(isManual isManual:Bool=false, isDone: (isDone:Bool) -> ()) {
        let stilFromSession = camSession.outputs.filter { (output) -> Bool in
            output.isKindOfClass(AVCaptureStillImageOutput)
        }
        
        guard let stil = stilFromSession.first as? AVCaptureStillImageOutput else {
            return
        }
        
        guard let vidCon = stil.connectionWithMediaType(AVMediaTypeVideo) else {
            return
        }
        
        let flashView = UIView(frame: self.view.frame)
        flashView.alpha = 0
        flashView.backgroundColor = UIColor.whiteColor()
        
        UIView.animateWithDuration(0.2, delay: 0, options: .Autoreverse, animations: { () -> Void in
            flashView.alpha = 0.9
            }, completion: { _ in
                flashView.removeFromSuperview()
        })
        
        dispatch_async(dispatch_get_main_queue()) {
            
            self.view.addSubview(flashView)
            
            stil.captureStillImageAsynchronouslyFromConnection(vidCon, completionHandler: { (imgBuffer, error) in
                if (error != nil) {
                    return
                }
                let imageData:NSData? = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imgBuffer)
                let imageCI:CIImage = CIImage(data: imageData!, options: [kCIImageColorSpace:NSNull()])!
                var feature:(rectangle: AnyObject?, isDetected: Bool)!
                if isManual {
                    let test = self.generateUIImage(self.rotateCIImage(imageCI))
                    let ga = CIImage(CGImage: test.CGImage!)
                    print("fr UI \(ga.extent)")
                    print("fr ORI \(imageCI.extent)")
                    feature = FeatureDetector(mode: .Capture, image: ga).doDetect()
                } else {
                    feature = FeatureDetector(mode: .Capture, image: imageCI).doDetect()
                }
                let originalImage = self.generateUIImage(self.rotateCIImage(imageCI))
                print("real ORI \(originalImage.size)")
                self.passThrough["ori"] = originalImage
                self.finishSession()
                
                if feature.isDetected {
                    guard let coordinat = feature.rectangle as? CIRectangleFeature else {
                        guard let _ = (feature.rectangle as? CIImage) else {
                            //you are fucked
                            isDone(isDone: true)
                            return
                        }
                        isDone(isDone: true)
                        return
                    }
                    
                    if self.captMode.selectedSegmentIndex == 0 {
                        let cropped = imageCI.imageByApplyingFilter("CIPerspectiveCorrection", withInputParameters: [
                            "inputTopLeft": CIVector(CGPoint: coordinat.topLeft),
                            "inputTopRight": CIVector(CGPoint: coordinat.topRight),
                            "inputBottomLeft": CIVector(CGPoint: coordinat.bottomLeft),
                            "inputBottomRight": CIVector(CGPoint: coordinat.bottomRight)
                            ]
                        )
                        self.passThrough["cropped"] = self.generateUIImage(self.rotateCIImage(cropped))
                        self.passThrough["coordinat"] = coordinat
                        isDone(isDone: true)
                        return
                    } else {
                        self.passThrough["coordinat"] = coordinat
                        isDone(isDone: true)
                        return
                    }
                }
                isDone(isDone: true)
                return
            })
        }
    }
    
    func rotateCIImage(imageCI: CIImage) -> CIImage {
        return CIImage().imageByApplyingFilter("CIAffineTransform",
                                               withInputParameters: ["inputImage": imageCI,
                                                "inputTransform": NSValue(CGAffineTransform: CGAffineTransformMakeRotation(CGFloat(-M_PI_2)))]
        )
    }
    
    func generateUIImage(imageCI: CIImage) -> UIImage {
        let ctx:CIContext = CIContext(options: Dictionary())
        let cgiRef:CGImageRef = ctx.createCGImage(imageCI, fromRect: imageCI.extent)
        return UIImage(CGImage: cgiRef)
    }
    
    func doSendImage(image: UIImage) {
        delegate.sendImage(image)
    }
    
    deinit {
        self.removeObserver(self, forKeyPath: "countVal")
    }
    
}

extension O2OCamView: O2OCamViewCloseDelegate {
    func closeAndUpload(image: UIImage) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: { 
            self.doSendImage(image)
        })
    }
}

extension O2OCamView: AVCaptureVideoDataOutputSampleBufferDelegate {
    public func captureOutput(captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!) {
        let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
        let opaqueBuffer = Unmanaged<CVImageBuffer>.passUnretained(imageBuffer!).toOpaque()
        let pixelBuffer = Unmanaged<CVPixelBuffer>.fromOpaque(opaqueBuffer).takeUnretainedValue()
        
        var outputImage = CIImage(CVPixelBuffer: pixelBuffer, options: nil)
        
        if self.captMode.selectedSegmentIndex == 0 {
            let rect = FeatureDetector(mode: self.mode, image: outputImage).doDetect()
            if rect.isDetected {
                self.countVal += 5
                outputImage = rect.rectangle as! CIImage
            }
        }
        
        if mode == .QR {
            outputImage = outputImage.imageByCroppingToRect(CGRect(x: videoFeed.drawableWidth/2-100, y: videoFeed.drawableHeight/2-100, width: 200, height: 200))
        }
        
        var drawFrame = outputImage.extent
        let imageAR = drawFrame.width / drawFrame.height
        let bound = CGRect(x: 0, y: 0, width: videoFeed.drawableWidth, height: videoFeed.drawableHeight)
        let viewAR = bound.width / bound.height
        if imageAR > viewAR {
            drawFrame.origin.x += (drawFrame.width - drawFrame.height * viewAR) / 2.0
            drawFrame.size.width = drawFrame.height / viewAR
        } else {
            drawFrame.origin.y += (drawFrame.height - drawFrame.width / viewAR) / 2.0
            drawFrame.size.height = drawFrame.width / viewAR
        }
        
        videoFeed.bindDrawable()
        if videoFeed.context != EAGLContext.currentContext() {
            EAGLContext.setCurrentContext(videoFeed.context)
        }
        
        // clear eagl view to grey
        glClearColor(0.5, 0.5, 0.5, 1.0);
        glClear(0x00004000)
        
        // set the blend mode to "source over" so that CI will use that
        glEnable(0x0BE2);
        glBlendFunc(1, 0x0303);
        
        renderContext.drawImage(outputImage, inRect: bound, fromRect: drawFrame)
        
        videoFeed.display()
    }
}

extension UIView {
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.CGPath
        self.layer.mask = mask
    }
}