//
//  Mode.swift
//  O2OcameraModule
//
//  Created by Afriyandi Setiawan on 7/13/16.
//  Copyright © 2016 Afriyandi Setiawan. All rights reserved.
//

import Foundation

public enum DetectMode {
    case QR
    case Detect
    case Capture
}