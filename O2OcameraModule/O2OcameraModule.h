//
//  O2OcameraModule.h
//  O2OcameraModule
//
//  Created by Afriyandi Setiawan on 6/23/16.
//  Copyright © 2016 Afriyandi Setiawan. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for O2OcameraModule.
FOUNDATION_EXPORT double O2OcameraModuleVersionNumber;

//! Project version string for O2OcameraModule.
FOUNDATION_EXPORT const unsigned char O2OcameraModuleVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <O2OcameraModule/PublicHeader.h>
#import "MADrawRect.h"

